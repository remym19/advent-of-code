import sys
from typing import List

PACKET_MARKER_LEN = 4
MESSAGE_MARKER_LEN = 14


class MarkerNotFoundException(Exception):
  pass


def count_chars_up_to_first_marker_end(char_stream: str, marker_len: int) -> int:
  """Recursive function that counts the number of chars up to the marker chars included.
  :param marker_length:
  """

  # Termination conditions
  if len(char_stream) < marker_len:
    raise MarkerNotFoundException

  elif is_a_marker(char_stream[:marker_len], marker_len):
    return marker_len

  # Recursive branch
  else:
    return 1 + count_chars_up_to_first_marker_end(char_stream[1:], marker_len)


def is_a_marker(candidate: str, marker_len) -> bool:
  """Defines what a marker"""
  distinct_chars_count = len(set(candidate))
  return distinct_chars_count == marker_len


def read_char_stream(filename: str):
  with open(filename, 'r') as reader:
    return reader.read()


if __name__ == '__main__':
  # Read the input files
  char_stream = read_char_stream('./puzzle_input.txt')

  # Setup system for the computations
  sys.setrecursionlimit(len(char_stream) * 3 // 2)  # The default recursion limit of 1000 was not sufficient

  # Compute the answers
  packet_answer = count_chars_up_to_first_marker_end(char_stream, PACKET_MARKER_LEN)
  message_answer = count_chars_up_to_first_marker_end(char_stream, MESSAGE_MARKER_LEN)

  # Print the answers
  print(f'Number of characters up to the end of the first packet marker: {packet_answer}')
  print(f'Number of characters up to the end of the first message marker: {message_answer}')
