from unittest import TestCase, main
from communication_tuning import *


class PacketMarkerDetectionTest(TestCase):
  def test_raisesException_withTooInputsShorterThanMarkerLength(self):
    self.assertRaises(MarkerNotFoundException, count_chars_up_to_first_marker_end, '', PACKET_MARKER_LEN)
    self.assertRaises(MarkerNotFoundException, count_chars_up_to_first_marker_end, 'a', PACKET_MARKER_LEN)
    self.assertRaises(MarkerNotFoundException, count_chars_up_to_first_marker_end, 'ab', PACKET_MARKER_LEN)
    self.assertRaises(MarkerNotFoundException, count_chars_up_to_first_marker_end, 'abc', PACKET_MARKER_LEN)

  def test_returns4_withMarkerAsInput(self):
    self.assertEqual(4, count_chars_up_to_first_marker_end('abcd', PACKET_MARKER_LEN))

  def test_returns4_withOtherMarkerAsInput(self):
    self.assertEqual(4, count_chars_up_to_first_marker_end('efgh', PACKET_MARKER_LEN))

  def test_returns5_withMarkerAtEndOfInput(self):
    self.assertEqual(5, count_chars_up_to_first_marker_end('aabcd', PACKET_MARKER_LEN))

  def test_returns5_withSingleCharAfterMarker(self):
    self.assertEqual(5, count_chars_up_to_first_marker_end('aabcde', PACKET_MARKER_LEN))

  def test_returns5_withTwoCharsAfterMarker(self):
    self.assertEqual(5, count_chars_up_to_first_marker_end('aabcdef', PACKET_MARKER_LEN))

  def test_returns5_withLastMarkerCharRepeatedAfterIt(self):
    self.assertEqual(5, count_chars_up_to_first_marker_end('aabcdd', PACKET_MARKER_LEN))

  def test_example_returns5(self):
    self.assertEqual(5, count_chars_up_to_first_marker_end('bvwbjplbgvbhsrlpgdmjqwftvncz', PACKET_MARKER_LEN))

  def test_example_returns6(self):
    self.assertEqual(6, count_chars_up_to_first_marker_end('nppdvjthqldpwncqszvftbrmjlhg', PACKET_MARKER_LEN))

  def test_example_returns10(self):
    self.assertEqual(10, count_chars_up_to_first_marker_end('nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg', PACKET_MARKER_LEN))

  def test_example_returns11(self):
    self.assertEqual(11, count_chars_up_to_first_marker_end('zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw', PACKET_MARKER_LEN))


class MessageMarkerDetectionTest(TestCase):
  def test_example_returns19(self):
    self.assertEqual(19, count_chars_up_to_first_marker_end('mjqjpqmgbljsphdztnvjfqwrcgsmlb', MESSAGE_MARKER_LEN))

  def test_example_returns23(self):
    self.assertEqual(23, count_chars_up_to_first_marker_end('bvwbjplbgvbhsrlpgdmjqwftvncz', MESSAGE_MARKER_LEN))

  def test_example_returns23_other(self):
    self.assertEqual(23, count_chars_up_to_first_marker_end('nppdvjthqldpwncqszvftbrmjlhg', MESSAGE_MARKER_LEN))

  def test_example_returns29(self):
    self.assertEqual(29, count_chars_up_to_first_marker_end('nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg', MESSAGE_MARKER_LEN))

  def test_example_returns26(self):
    self.assertEqual(26, count_chars_up_to_first_marker_end('zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw', MESSAGE_MARKER_LEN))


if __name__ == '__main__':
  main()
