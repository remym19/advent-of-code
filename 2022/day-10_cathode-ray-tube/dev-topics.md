Dev topics:

- OOP
- Encapsulation via properties and private methods
- Observer pattern
- Documentation