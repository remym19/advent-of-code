import logging
import sys
from abc import ABC, abstractmethod
from pathlib import Path
from typing import List, Union, Optional, Set


class ClockObserver(ABC):
    """
    Observer interface for the clock
    """

    def clock_tick_event(self) -> None:
        """
        Notify the observer that the clock has ticked
        """
        pass

    def clock_in_cycle_after_tick_event(self) -> None:
        """
        Notify the observer that the clock is inside a cycle after a tick
        """
        pass


class Clock:
    """
    A clock that notifies observers on each tick.
    The ticks are simulated by calling its new_ticks method.
    It keeps track of the number of ticks that have passed.
    """
    TICK_COUNT_START_VALUE: int = 0

    def __init__(self) -> None:
        super().__init__()
        self.observers: Set[ClockObserver] = set()
        self.__tick_count: int = self.__class__.TICK_COUNT_START_VALUE

    def __str__(self) -> str:
        return self.__repr__()

    def __repr__(self) -> str:
        return f"Clock(tick_count={self.__tick_count})"

    def register(self, clock_observer: ClockObserver) -> 'Clock':
        """
        Register an observer to be notified on each tick
        :param clock_observer: The observer to register
        :return: The clock itself
        """
        self.observers.add(clock_observer)
        return self

    def new_ticks(self, quantity: int) -> None:
        """
        Notify all observers of new ticks
        :param quantity: The number of ticks to notify
        """
        logging.debug(self.observers)
        for _ in range(quantity):
            self.__tick_count += 1
            self.__notify_tick()
            self.__notify_after_tick()

    def __notify_tick(self) -> None:
        for o in self.observers:
            o.clock_tick_event()

    def __notify_after_tick(self) -> None:
        for o in self.observers:
            o.clock_in_cycle_after_tick_event()


class CpuInstruction(ABC):
    """
    Interface for CPU instructions
    """

    @abstractmethod
    def reset(self) -> None:
        pass

    @abstractmethod
    def execute_one_cycle(self, cpu: 'Cpu') -> None:
        pass

    @abstractmethod
    def is_finished(self) -> bool:
        pass


class Addx(CpuInstruction):
    """
    CPU instruction that adds a given value to the X register
    """

    TOTAL_CYCLES = 2

    def __init__(self, value: int) -> None:
        super().__init__()
        self.__value: int = value
        self.executed_cycles: int = 0

    def __eq__(self, other):
        return isinstance(other, self.__class__) and \
            self.__value == other.__value

    def __str__(self) -> str:
        return self.__repr__()

    def __repr__(self) -> str:
        return f"Addx(value={self.__value},executed_cycles={self.executed_cycles})"

    def reset(self) -> None:
        self.executed_cycles: int = 0

    def execute_one_cycle(self, cpu: 'Cpu') -> None:
        self.executed_cycles += 1
        if self.is_finished():
            cpu.x_register += self.__value

    def is_finished(self) -> bool:
        return self.executed_cycles == self.__class__.TOTAL_CYCLES


class Noop(CpuInstruction):
    """
    CPU instruction that does nothing
    """

    def __init__(self) -> None:
        super().__init__()
        self.finished = False

    def __eq__(self, other):
        return isinstance(other, self.__class__) and \
            self.finished == other.finished

    def __str__(self) -> str:
        return self.__repr__()

    def __repr__(self) -> str:
        return f"Noop()"

    def reset(self) -> None:
        self.finished = False

    def execute_one_cycle(self, cpu: 'Cpu') -> None:
        self.finished = True

    def is_finished(self) -> bool:
        return self.finished


class Cpu(ClockObserver):
    """
    A CPU that executes instructions
    """

    X_REGISTER_START_VALUE: int = 1

    def __init__(self, clock: 'Clock') -> None:
        super().__init__()
        self.__x_register: int = self.__class__.X_REGISTER_START_VALUE
        self.__current_instruction = None
        self.__instructions_queue: List[CpuInstruction] = []
        self.__instructions_trace: List[CpuInstruction] = []
        self.__clock = clock
        self.__clock.register(self)

    def __str__(self) -> str:
        return self.__repr__()

    def __repr__(self) -> str:
        return f"Cpu(" \
               f"x_register={self.__x_register}," \
               f"current_instruction={self.__current_instruction}" \
               f")"

    @property
    def x_register(self) -> int:
        return self.__x_register

    @x_register.setter
    def x_register(self, value: int) -> None:
        self.__x_register = value

    @property
    def current_instruction(self) -> CpuInstruction:
        return self.__current_instruction

    @property
    def instructions_trace(self) -> List[CpuInstruction]:
        return self.__instructions_trace

    def load(self, program: List[CpuInstruction]) -> None:
        self.__instructions_queue.extend(program)

    def clock_tick_event(self) -> None:
        if len(self.__instructions_queue) > 0 or self.__current_instruction:
            if not self.__current_instruction:
                self.__next_instruction()

            if not self.__current_instruction.is_finished():
                self.__current_instruction.execute_one_cycle(self)

            if self.__current_instruction.is_finished():
                self.__end_current_instruction()

    def __next_instruction(self):
        self.__current_instruction = self.__instructions_queue.pop(0)
        if self.__current_instruction:
            self.__current_instruction.reset()

    def __end_current_instruction(self):
        self.__instructions_trace.append(self.__current_instruction)
        self.__current_instruction = None


class ProgramParser:
    """
    Parses a program file into a list of CPU instructions
    """

    @classmethod
    def parse_file(cls, path: Union[str, Path]) -> List[CpuInstruction]:
        """
        Parse a program file into a list of CPU instructions
        :param path: The path to the program file
        :return:     A list of CPU instructions
        """
        path = Path(path)
        lines = path.read_text().splitlines()
        return cls.parse_lines(lines)

    @classmethod
    def parse_lines(cls, lines: List[str]) -> List[CpuInstruction]:
        """
        Parse a list of lines into a list of CPU instructions
        :param lines: The lines to parse
        :return:      A list of CPU instructions
        """
        instructions = []
        for line in lines:
            parsed = cls.__parse_instruction(line)
            if parsed:
                instructions.append(parsed)
        return instructions

    @classmethod
    def __parse_instruction(cls, line: str) -> Optional[CpuInstruction]:
        if line == "noop":
            return Noop()
        elif line.startswith("addx "):
            return cls.__parse_addx_instruction(line)
        return None

    @classmethod
    def __parse_addx_instruction(cls, string: str) -> Optional[CpuInstruction]:
        value_str = string.split(" ")[1]
        try:
            value = int(value_str)
            return Addx(value)
        except ValueError:
            print(f"WARN: Ignoring instruction: <{string}>, unrecognized value: <{value_str}>.", file=sys.stderr)
            return None


class Part1Solver:
    @classmethod
    def print_strength_sum(cls, program_path: Union[str, Path]) -> None:
        """
        Compute the sum of the signal strength during cycles 20, 60, 100, 140, 180, and 220.
        :param program_path: The path to the program file
        """
        logging.basicConfig(level=logging.INFO, stream=sys.stdout, format='%(levelname)s: %(message)s')

        logging.info(f"--- {program_path=} ---")
        program = ProgramParser.parse_file(program_path)

        # Given the input data, it is easier to compute the strength from the first cycle each time.
        strength_sum = sum(
            cls.__get_signal_strength_during_cycle(program, c)
            for c in [20, 60, 100, 140, 180, 220]
        )

        logging.info(f"{strength_sum=}")

    @classmethod
    def __get_signal_strength_during_cycle(cls, program: List[CpuInstruction], cycle: int) -> int:
        """
        Get the signal strength during a given cycle, i.e. after the given cycle starts but before the next one starts.
        :param program: The program to load into the CPU
        :param cycle:   The cycle to get the signal strength during
        :return:        The signal strength during the given cycle
        """
        cpu = cls.__get_cpu_during(program, cycle)
        strength = cycle * cpu.x_register
        logging.debug(f"{cycle=}\t{cpu.x_register=}\t{strength=}")
        return strength

    @classmethod
    def __get_cpu_during(cls, program: List[CpuInstruction], cycle: int) -> Cpu:
        """
        Get the state of the CPU during a given cycle, i.e. after the given cycle starts but before the next one starts.
        :param program: The program to load into the CPU
        :param cycle:   The cycle to get the CPU state during
        :return:        The CPU state during the given cycle
        """
        clock = Clock()
        cpu = Cpu(clock)
        cpu.load(program)
        # -1 to position the cpu state during the cycle, rather than during the next one
        clock.new_ticks(cycle - 1)
        return cpu


class CpuReader:
    """
    Reads the state of the CPU
    """

    def __init__(self, cpu: Cpu) -> None:
        self.__cpu = cpu

    def read_x_register(self) -> int:
        """
        Read the value of the CPU's X register
        :return: The value of the CPU's X register
        """
        return self.__cpu.x_register


class CrtScreen(ClockObserver):
    # Dimensions
    WIDTH = 40
    HEIGHT = 6

    # Characters
    LIT_PIXEL = '#'
    DARK_PIXEL = '.'
    INITIAL_PIXEL = '_'

    def __init__(self, clock_: Clock, cpu_reader: CpuReader) -> None:
        # State
        self._pixels = [
            [self.INITIAL_PIXEL for _ in range(self.WIDTH)]  # columns
            for _ in range(self.HEIGHT)  # rows
        ]
        self.__x = 0  # row, left-to-right
        self.__y = 0  # column, top-to-bottom
        self.__sprite_x = [0, 1, 2]  # sprite area on the current row

        # Dependencies
        self.__clock = clock_
        self.__clock.register(self)
        self.__cpu_reader = cpu_reader

    def __str__(self) -> str:
        return "\n".join(
            "".join(row) for row in self._pixels
        )

    def clock_in_cycle_after_tick_event(self) -> None:
        logging.debug(f"{self.__sprite_x=}")
        self.__draw_current_pixel()
        self.__advance_sprite()
        self.__advance_pixel()

    def __draw_current_pixel(self) -> None:
        self._pixels[self.__y][self.__x] = self.LIT_PIXEL if self.__x in self.__sprite_x else self.DARK_PIXEL

    def __advance_pixel(self) -> None:
        self.__x = (self.__x + 1) % self.WIDTH
        # When the pixel reached the end of the row, advance to the next row.
        if self.__x == 0:
            self.__y = (self.__y + 1) % self.HEIGHT

    def __advance_sprite(self) -> None:
        x_register = self.__cpu_reader.read_x_register()
        self.__sprite_x = [x_register - 1, x_register, x_register + 1]

        candidate_x = [x_register - 1, x_register, x_register + 1]
        self.__sprite_x = [max(0, min(self.WIDTH - 1, c)) for c in candidate_x]

        assert max(self.__sprite_x) < self.WIDTH, f"{self.__sprite_x=}, {self.WIDTH=}"
        assert min(self.__sprite_x) >= 0, f"{self.__sprite_x=}, {self.WIDTH=}"


class Part2Solver:
    @staticmethod
    def print_screen(program_path: Union[str, Path]) -> None:
        """

        :param program_path: The path to the program file
        """
        logging.basicConfig(level=logging.DEBUG, stream=sys.stdout, format='%(levelname)s: %(message)s')

        logging.info(f"--- {program_path=} ---")
        program = ProgramParser.parse_file(program_path)

        clock = Clock()
        cpu = Cpu(clock)
        cpu.load(program)
        screen = CrtScreen(clock, CpuReader(cpu))
        clock.new_ticks(CrtScreen.WIDTH * CrtScreen.HEIGHT)
        logging.info(f"screen:\n{screen}")


if __name__ == '__main__':
    Part1Solver.print_strength_sum("example.txt")
    Part1Solver.print_strength_sum("puzzle-input.txt")

    Part2Solver.print_screen("example.txt")

    CrtScreen.LIT_PIXEL = '###'  # To make the puzzle output easier to read
    CrtScreen.DARK_PIXEL = '   '  # To make the puzzle output easier to read
    Part2Solver.print_screen("puzzle-input.txt")
