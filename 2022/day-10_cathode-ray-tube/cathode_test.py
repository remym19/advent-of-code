from unittest import TestCase

from cathode import *

logging.basicConfig(level=logging.INFO)


class CpuTest(TestCase):

    def setUp(self) -> None:
        super().setUp()
        self.clock = Clock()
        self.cpu = Cpu(self.clock)
        self.noop = Noop()

    @staticmethod
    def addx(value: int) -> Addx:
        return Addx(value)

    def test_tick_ignored_if_no_instructions(self):
        self.assertEqual(Cpu.X_REGISTER_START_VALUE, self.cpu.x_register)

        self.cpu.load([])
        self.clock.new_ticks(1)

        self.assertEqual(Cpu.X_REGISTER_START_VALUE, self.cpu.x_register)
        self.assertEqual(None, self.cpu.current_instruction)
        self.assertEqual([], self.cpu.instructions_trace)

    def test_single_noop_instruction(self):
        self.assertEqual(Cpu.X_REGISTER_START_VALUE, self.cpu.x_register)

        self.cpu.load([self.noop])
        self.clock.new_ticks(1)

        self.assertEqual(Cpu.X_REGISTER_START_VALUE, self.cpu.x_register)
        self.assertEqual(None, self.cpu.current_instruction)
        self.assertEqual([self.noop], self.cpu.instructions_trace)

    def test_cumulate_instructions(self):
        self.assertEqual(Cpu.X_REGISTER_START_VALUE, self.cpu.x_register)

        self.cpu.load([self.noop])
        self.clock.new_ticks(1)
        self.cpu.load([self.noop])
        self.clock.new_ticks(1)

        self.assertEqual(Cpu.X_REGISTER_START_VALUE, self.cpu.x_register)
        self.assertEqual(None, self.cpu.current_instruction)
        self.assertEqual([self.noop, self.noop], self.cpu.instructions_trace)

    def test_does_not_execute_instruction_without_tick(self):
        self.assertEqual(Cpu.X_REGISTER_START_VALUE, self.cpu.x_register)

        self.cpu.load([self.noop])

        self.assertEqual(Cpu.X_REGISTER_START_VALUE, self.cpu.x_register)
        self.assertEqual(None, self.cpu.current_instruction)
        self.assertEqual([], self.cpu.instructions_trace)

    def test_execute_single_noop_for_1_tick(self):
        self.assertEqual(Cpu.X_REGISTER_START_VALUE, self.cpu.x_register)

        self.cpu.load([self.noop, self.noop])
        self.clock.new_ticks(1)

        self.assertEqual(Cpu.X_REGISTER_START_VALUE, self.cpu.x_register)
        self.assertEqual(None, self.cpu.current_instruction)
        self.assertEqual([self.noop], self.cpu.instructions_trace)

    def test_executing_single_addx_with_1_tick(self):
        addx_7 = self.addx(7)
        self.assertEqual(Cpu.X_REGISTER_START_VALUE, self.cpu.x_register)

        self.cpu.load([addx_7])
        self.clock.new_ticks(1)

        self.assertEqual(addx_7, self.cpu.current_instruction)
        self.assertEqual([], self.cpu.instructions_trace)
        self.assertEqual(Cpu.X_REGISTER_START_VALUE, self.cpu.x_register)

    def test_executed_single_addx_with_2_ticks(self):
        addx_7 = self.addx(7)
        self.assertEqual(Cpu.X_REGISTER_START_VALUE, self.cpu.x_register)

        self.cpu.load([addx_7])
        self.clock.new_ticks(2)

        self.assertEqual(None, self.cpu.current_instruction)
        self.assertEqual([addx_7], self.cpu.instructions_trace)
        self.assertEqual(Cpu.X_REGISTER_START_VALUE + 7, self.cpu.x_register)


class ParserTest(TestCase):
    def test_parse_noop(self):
        result = ProgramParser.parse_lines(["noop"])
        self.assertEqual([Noop()], result)

    def test_parse_addx_3(self):
        result = ProgramParser.parse_lines(["addx 3"])
        self.assertEqual([Addx(3)], result)

    def test_ignores_addx_without_value(self):
        result = ProgramParser.parse_lines(["addx "])
        self.assertEqual([], result)

    def test_ignores_addx_with_non_numeric_value(self):
        result = ProgramParser.parse_lines(["addx _"])
        self.assertEqual([], result)

    def test_ignores_addx_with_non_int_numeric_value(self):
        result = ProgramParser.parse_lines(["addx 1.0"])
        self.assertEqual([], result)


class CrtScreenTest(TestCase):
    def test_screen_coordinates(self):
        # Given
        clock = Clock()
        cpu = Cpu(clock)
        CrtScreen.INITIAL_PIXEL = '.'
        screen = CrtScreen(clock, CpuReader(cpu))

        # When
        screen._pixels[0][3] = 'C'  # first row, given column
        screen._pixels[3][0] = 'R'  # first column, given row

        # Then
        print("Result:")
        print(screen)
        screen_lines = str(screen).splitlines()
        self.assertEqual("...C", screen_lines[0][0:4])
        self.assertEqual("R...", screen_lines[3][0:4])

    def test_example(self):
        # Given
        program = ProgramParser.parse_file("example.txt")
        clock = Clock()
        cpu = Cpu(clock)
        screen = CrtScreen(clock, CpuReader(cpu))
        cpu.load(program)

        # When
        clock.new_ticks(40 * 6)

        # Then
        expected = """
##..##..##..##..##..##..##..##..##..##..
###...###...###...###...###...###...###.
####....####....####....####....####....
#####.....#####.....#####.....#####.....
######......######......######......####
#######.......#######.......#######.....
""".strip()
        result = str(screen).strip()
        print(f"Result:\n{result}")
        self.assertEqual(expected, result)
