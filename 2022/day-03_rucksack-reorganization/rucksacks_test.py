from unittest import TestCase
from rucksacks import *

# Upper case and lower case letters represent different items and don't have the same priority.
# Therefore, the lower case items aren't capitalized to differentiate them from the upper case items.

PRIORITY_a = 1
PRIORITY_c = 3
PRIORITY_d = 4
PRIORITY_e = 5
PRIORITY_p = 16
PRIORITY_r = 18
PRIORITY_s = 19
PRIORITY_t = 20
PRIORITY_v = 22

PRIORITY_A = 27
PRIORITY_D = 30
PRIORITY_L = 38
PRIORITY_P = 42
PRIORITY_Z = 52


class MisplacedItemsTest(TestCase):
  def test_empty_string(self):
    self.assertEqual(0, misplaced_items_priority_sum(['']))

  def test_no_item_in_both_compartements(self):
    self.assertEqual(0, misplaced_items_priority_sum(['ab']))

  def test_single_item(self):
    self.assertEqual(PRIORITY_c, misplaced_items_priority_sum(['cc']))

  def test_two_items_c(self):
    self.assertEqual(PRIORITY_c, misplaced_items_priority_sum(['acBc']))

  def test_two_items_d(self):
    self.assertEqual(PRIORITY_d, misplaced_items_priority_sum(['adCd']))

  def test_two_items_e(self):
    self.assertEqual(PRIORITY_e, misplaced_items_priority_sum(['abeAeB']))

  def test_two_items_Z_of_max_priority(self):
    self.assertEqual(PRIORITY_Z, misplaced_items_priority_sum(['ABCZZabc']))

  def test_two_items_not_Z_of_max_priority(self):
    self.assertEqual(PRIORITY_D, misplaced_items_priority_sum(['abcDghDZ']))

  def test_example_with_item_p(self):
    self.assertEqual(PRIORITY_p, misplaced_items_priority_sum(['vJrwpWtwJgWrhcsFMMfFFhFp']))

  def test_example_with_item_L(self):
    self.assertEqual(PRIORITY_L, misplaced_items_priority_sum(['jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL']))

  def test_example_with_item_P(self):
    self.assertEqual(PRIORITY_P, misplaced_items_priority_sum(['PmmdzqPrVvPwwTWBwg']))

  def test_example_with_item_v(self):
    self.assertEqual(PRIORITY_v, misplaced_items_priority_sum(['wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn']))

  def test_example_with_item_t(self):
    self.assertEqual(PRIORITY_t, misplaced_items_priority_sum(['ttgJtRGJQctTZtZT']))

  def test_example_with_item_s(self):
    self.assertEqual(PRIORITY_s, misplaced_items_priority_sum(['CrZsJsPPZsGzwwsLwLmpwMDw']))

  def test_sum_with_two_simple_rucksacks(self):
    rucksacks = ['acBc', 'adCd']
    result = misplaced_items_priority_sum(rucksacks)
    expected = PRIORITY_c + PRIORITY_d
    self.assertEqual(expected, result)

  def test_sum_with_three_simple_rucksacks(self):
    rucksacks = ['acBc', 'adCd', 'abeAeB']
    expected = PRIORITY_c + PRIORITY_d + PRIORITY_e
    self.assertEqual(expected, misplaced_items_priority_sum(rucksacks))

  def test_sum_from_file(self):
    self.assertEqual(157, misplaced_items_priority_sum_from_file('test_data/example-misplaced-items-sum-157.txt'))


class GroupBadgesTest(TestCase):
  def test_single_group_badge_a(self):
    self.assertEqual(PRIORITY_a, badges_priority_sum_from_file('test_data/single-group_two-items_badge-a.txt'))

  def test_single_group_badge_D(self):
    self.assertEqual(PRIORITY_D, badges_priority_sum_from_file('test_data/single-group_two-items_badge-D.txt'))

  def test_example_first_group(self):
    self.assertEqual(PRIORITY_r, badges_priority_sum_from_file(
      'test_data/example-first-group_badge-r.txt'))

  def test_two_group_badges_e_L(self):
    self.assertEqual(PRIORITY_e + PRIORITY_L, badges_priority_sum_from_file('test_data/two-groups_badges-e-L.txt'))

  def test_example_two_groups(self):
    self.assertEqual(PRIORITY_r + PRIORITY_Z, badges_priority_sum_from_file(
      'test_data/example-two-groups_badge-r-Z.txt'))
