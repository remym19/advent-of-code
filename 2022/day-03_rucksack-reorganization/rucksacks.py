from typing import List


def badges_priority_sum_from_file(filename: str):
  rucksacks_str_list = read_file_lines(filename)

  GROUP_SIZE = 3
  rucksacks_count = len(rucksacks_str_list)
  group_start_indices = range(0, rucksacks_count, GROUP_SIZE)

  groups = [rucksacks_str_list[i:i + GROUP_SIZE]
            for i in group_start_indices]

  badges = map(badge_of_group, groups)
  priorities = map(priority, badges)
  return sum(priorities)


def badge_of_group(group_rucksacks_str_list):
  # There are 3 rucksacks per group.
  r1, r2, r3 = group_rucksacks_str_list

  # The badge of a group is the single item (type) that is in all rucksacks.
  for item1 in r1:
    for item2 in r2:
      for item3 in r3:

        if item1 == item2 == item3:
          return item1

  return ''  # Fallback value, no common item


def misplaced_items_priority_sum_from_file(filename: str):
  rucksacks_str_list = read_file_lines(filename)
  return misplaced_items_priority_sum(rucksacks_str_list)


def misplaced_items_priority_sum(rucksacks_str_list: List[str]):
  # Get the priority of the item in both compartment,
  # For each rucksack,
  # And sum the priorities.
  items = map(misplaced_item, rucksacks_str_list)
  priorities = map(priority, items)
  return sum(priorities)


def misplaced_item(rucksack_str):
  total_count = len(rucksack_str)
  assert total_count % 2 == 0  # Because the number of items in both compartments is the same.
  half_count = total_count // 2

  compartment1 = rucksack_str[:half_count]
  compartment2 = rucksack_str[half_count:]

  # The misplaced item is the single item that is in both compartments.
  for item in compartment1:
    if item in compartment2:
      return item

  return ''  # Fallback value


def priority(letter: str):
  if len(letter) != 1:  # Not a single character
    return 0  # Fallback value

  def letter_order_from(start):
    return ord(letter) - ord(start) + 1

  if 'a' <= letter <= 'z':
    return letter_order_from('a')
  elif 'A' <= letter <= 'Z':
    return priority('z') + letter_order_from('A')
  else:  # Unrecognized letter
    return 0  # Fallback value


def read_file_lines(filename):
  with open(filename, 'r') as reader:
    lines = reader.read().splitlines()
  return lines


if __name__ == '__main__':
  misplaced_items_answer = misplaced_items_priority_sum_from_file('./puzzle_input.txt')
  badges_answer = badges_priority_sum_from_file('./puzzle_input.txt')

  print(f'Rucksack misplaced items priority sum : {misplaced_items_answer}')
  print(f'Badges priority sum : {badges_answer}')
