from typing import List

ROCK, PAPER, SCISSORS = range(3)  # Shapes enumeration
ROCK_POINTS, PAPER_POINTS, SCISSORS_POINTS = 1, 2, 3  # Shape points

WIN, DRAW, LOSE = range(3)  # Outcomes enumeration
WIN_POINTS, DRAW_POINTS, LOSE_POINTS = 6, 3, 0  # Outcome points


def total_score_outcomes_against_opponents_from_file(filename: str):
  # Case of interpreting the second letter as the outcome to obtain
  opponent_and_outcome_str_list = read_lines(filename)
  outcome_against_opponent_list = map(parse_outcome_against_opponent, opponent_and_outcome_str_list)
  scores = map(score_outcome_against_opponent, outcome_against_opponent_list)
  total_score = sum(scores)
  return total_score


def total_score_outcomes_against_opponents_from_str_list(opponent_and_outcome_str_list: List[str]):
  outcome_against_opponent_list = map(parse_outcome_against_opponent, opponent_and_outcome_str_list)
  scores = map(score_outcome_against_opponent, outcome_against_opponent_list)
  total_score = sum(scores)
  return total_score


def score_outcome_against_opponent(outcome_against_opponent):
  [outcome, opponent] = outcome_against_opponent
  player = shape_for_outcome_against_opponent(outcome, opponent)
  score = outcome_points(outcome) + shape_points(player)
  return score


def shape_for_outcome_against_opponent(outcome, opponent):
  def shape_losing_against(other):
    if other == ROCK:
      return SCISSORS
    elif other == PAPER:
      return ROCK
    elif other == SCISSORS:
      return PAPER

  def shape_winning_against(other):
    if other == ROCK:
      return PAPER
    elif other == PAPER:
      return SCISSORS
    elif other == SCISSORS:
      return ROCK

  if outcome == LOSE:
    return shape_losing_against(opponent)
  elif outcome == DRAW:
    return opponent
  elif outcome == WIN:
    return shape_winning_against(opponent)


def total_score_player_against_opponent_from_file(filename: str):
  # Case of interpreting the second letter as the shape to play
  shape_strategies = read_lines(filename)
  return total_score_player_against_opponent_from_str_list(shape_strategies)


def total_score_player_against_opponent_from_str_list(player_against_opponent_str_list: List[str]):
  player_against_opponent_list = map(parse_player_and_opponent, player_against_opponent_str_list)
  scores = map(score_player_against_opponent, player_against_opponent_list)
  total_score = sum(scores)
  return total_score


def score_player_against_opponent(player_against_opponent):
  [player, opponent] = player_against_opponent
  return shape_points(player) + score_outcome(player, opponent)


def read_lines(filename):
  with open(filename, 'r') as reader:
    lines = reader.read().splitlines()
  return lines


def parse_outcome_against_opponent(opponent_and_outcome_str):
  opponent_str, outcome_str = opponent_and_outcome_str.split()
  opponent = parse_shape(opponent_str)
  outcome = parse_outcome(outcome_str)
  return [outcome, opponent]


def parse_player_and_opponent(player_against_opponent_str: str):
  opponent_str, player_str = player_against_opponent_str.split()
  opponent = parse_shape(opponent_str)
  player = parse_shape(player_str)
  return player, opponent


def parse_shape(shape_str):
  if shape_str in ['A', 'X']:
    return ROCK
  elif shape_str in ['B', 'Y']:
    return PAPER
  elif shape_str in ['C', 'Z']:
    return SCISSORS
  else:
    raise ValueError(f'Unrecognized shape string: {shape_str}')


def parse_outcome(outcome_str):
  if outcome_str == 'X':
    return LOSE
  if outcome_str == 'Y':
    return DRAW
  if outcome_str == 'Z':
    return WIN


def score_outcome(player, opponent):
  def greater_than(shape1, shape2):
    # True if greater
    # False if lower OR EQUAL
    return shape1 == ROCK and shape2 == SCISSORS or \
      shape1 == PAPER and shape2 == ROCK or \
      shape1 == SCISSORS and shape2 == PAPER

  if greater_than(player, opponent):
    return WIN_POINTS
  elif opponent == player:
    return DRAW_POINTS
  elif greater_than(opponent, player):
    return LOSE_POINTS
  else:
    raise ValueError(f'Unrecognized shape: {opponent} or {player}')


def shape_points(shape):
  if shape == ROCK:
    return ROCK_POINTS
  elif shape == PAPER:
    return PAPER_POINTS
  elif shape == SCISSORS:
    return SCISSORS_POINTS
  else:
    raise ValueError(f'Unrecognized shape: {shape}')


def outcome_points(outcome):
  if outcome == LOSE:
    return LOSE_POINTS
  elif outcome == DRAW:
    return DRAW_POINTS
  elif outcome == WIN:
    return WIN_POINTS
  else:
    raise ValueError(f'Unrecognized outcome: {outcome}')


if __name__ == '__main__':
  shape_strategies_answer = total_score_player_against_opponent_from_file('./puzzle_input.txt')
  outcome_strategies_answer = total_score_outcomes_against_opponents_from_file('./puzzle_input.txt')
  print(f'Total score with the shape strategies : {shape_strategies_answer}')
  print(f'Total score with the outcome strategies : {outcome_strategies_answer}')
