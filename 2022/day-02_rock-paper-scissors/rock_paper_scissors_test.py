from unittest import TestCase
from rock_paper_scissors import *

# RockPaperScissorsTest

# String format :
# - the first letter is the opponent' shape string
# - the second letter is either the player'rucksack_str shape string, or the player'rucksack_str desired outcome

# Expected score per player_against_opponent_str_list
# round_score = shape_points + outcome_points
WIN_WITH_ROCK = 6 + 1
WIN_WITH_PAPER = 6 + 2
WIN_WITH_SCISSORS = 6 + 3
DRAW_WITH_ROCK = 3 + 1
DRAW_WITH_PAPER = 3 + 2
DRAW_WITH_SCISSORS = 3 + 3
LOSE_WITH_ROCK = 0 + 1
LOSE_WITH_PAPER = 0 + 2
LOSE_WITH_SCISSORS = 0 + 3


class StrategyScoringTest(TestCase):
  def test_score_from_file(self):
    result = total_score_outcomes_against_opponents_from_file('test_data/multiple-outcome-strategy-score-22.txt')
    expected = LOSE_WITH_SCISSORS + LOSE_WITH_PAPER + DRAW_WITH_ROCK + WIN_WITH_PAPER + DRAW_WITH_PAPER
    self.assertEqual(expected, result)

  def test_two_outcomes(self):
    self.assertEqual(LOSE_WITH_ROCK + DRAW_WITH_SCISSORS,
                     total_score_outcomes_against_opponents_from_str_list(['B X', 'C Y']))

  def test_three_outcomes(self):
    self.assertEqual(LOSE_WITH_ROCK + DRAW_WITH_SCISSORS + WIN_WITH_PAPER,
                     total_score_outcomes_against_opponents_from_str_list(['B X', 'C Y', 'A Z']))

  def test_lose_with_rock(self):
    self.assertEqual(LOSE_WITH_ROCK, total_score_outcomes_against_opponents_from_str_list(['B X']))

  def test_lose_with_paper(self):
    self.assertEqual(LOSE_WITH_PAPER, total_score_outcomes_against_opponents_from_str_list(['C X']))

  def test_lose_with_scissors(self):
    self.assertEqual(LOSE_WITH_SCISSORS, total_score_outcomes_against_opponents_from_str_list(['A X']))

  def test_draw_with_rock(self):
    self.assertEqual(DRAW_WITH_ROCK, total_score_outcomes_against_opponents_from_str_list(['A Y']))

  def test_draw_with_paper(self):
    self.assertEqual(DRAW_WITH_PAPER, total_score_outcomes_against_opponents_from_str_list(['B Y']))

  def test_draw_with_scissors(self):
    self.assertEqual(DRAW_WITH_SCISSORS, total_score_outcomes_against_opponents_from_str_list(['C Y']))

  def test_win_with_rock(self):
    self.assertEqual(WIN_WITH_ROCK, total_score_outcomes_against_opponents_from_str_list(['C Z']))

  def test_win_with_paper(self):
    self.assertEqual(WIN_WITH_PAPER, total_score_outcomes_against_opponents_from_str_list(['A Z']))

  def test_win_with_scissors(self):
    self.assertEqual(WIN_WITH_SCISSORS, total_score_outcomes_against_opponents_from_str_list(['B Z']))

  def test_ValueError_cases(self):
    self.assertRaises(ValueError, total_score_outcomes_against_opponents_from_str_list, ['A W'])
    self.assertRaises(ValueError, total_score_outcomes_against_opponents_from_str_list, ['D X'])
    self.assertRaises(ValueError, total_score_outcomes_against_opponents_from_str_list, ['A '])
    self.assertRaises(ValueError, total_score_outcomes_against_opponents_from_str_list, [' X'])
    self.assertRaises(ValueError, total_score_outcomes_against_opponents_from_str_list, [' '])


class TotalScoreFromFileTest(TestCase):
  def test_multiple_rounds(self):
    result = total_score_player_against_opponent_from_file('test_data/multiple-shape-strategies-score-41.txt')
    expected = DRAW_WITH_ROCK + WIN_WITH_ROCK + WIN_WITH_PAPER + LOSE_WITH_SCISSORS + DRAW_WITH_PAPER
    self.assertEqual(expected, result)


class MultipleRoundsFromStringTest(TestCase):
  def test_2_draws_with_rocks(self):
    result = total_score_player_against_opponent_from_str_list(['A X', 'A X'])
    self.assertEqual(2 * DRAW_WITH_ROCK, result)

  def test_multiple_rounds(self):
    result = total_score_player_against_opponent_from_str_list([
      'A X', 'A X',
      'B X',
      'B Z', 'B Z',
      'C X',
      'C Z',
    ])
    self.assertEqual(
      2 * DRAW_WITH_ROCK +
      LOSE_WITH_ROCK +
      2 * WIN_WITH_SCISSORS +
      WIN_WITH_ROCK +
      DRAW_WITH_SCISSORS
      , result)


class DrawTest(TestCase):
  def test_draw_with_rocks(self):
    result = total_score_player_against_opponent_from_str_list(['A X'])
    self.assertEqual(DRAW_WITH_ROCK, result)

  def test_draw_with_paper(self):
    result = total_score_player_against_opponent_from_str_list(['B Y'])
    self.assertEqual(DRAW_WITH_PAPER, result)

  def test_draw_with_scissors(self):
    result = total_score_player_against_opponent_from_str_list(['C Z'])
    self.assertEqual(DRAW_WITH_SCISSORS, result)


class WinTest(TestCase):
  def test_win_with_rock_against_scissors(self):
    result = total_score_player_against_opponent_from_str_list(['C X'])
    self.assertEqual(WIN_WITH_ROCK, result)

  def test_win_with_paper_against_rock(self):
    result = total_score_player_against_opponent_from_str_list(['A Y'])
    self.assertEqual(WIN_WITH_PAPER, result)

  def test_win_with_scissors_against_paper(self):
    result = total_score_player_against_opponent_from_str_list(['B Z'])
    self.assertEqual(WIN_WITH_SCISSORS, result)


class LoseTest(TestCase):
  def test_lose_with_scissors_against_rock(self):
    result = total_score_player_against_opponent_from_str_list(['B X'])
    self.assertEqual(LOSE_WITH_ROCK, result)

  def test_lose_with_paper_against_rock(self):
    result = total_score_player_against_opponent_from_str_list(['C Y'])
    self.assertEqual(LOSE_WITH_PAPER, result)

  def test_lose_with_scissors_against_paper(self):
    result = total_score_player_against_opponent_from_str_list(['A Z'])
    self.assertEqual(LOSE_WITH_SCISSORS, result)


class InvalidStringTest(TestCase):
  def test_ValueError_without_second_hand(self):
    self.assertRaises(ValueError, total_score_player_against_opponent_from_str_list, 'A ')

  def test_ValueError_without_first_hand(self):
    self.assertRaises(ValueError, total_score_player_against_opponent_from_str_list, ' X')

  def test_ValueError_without_both_hands(self):
    self.assertRaises(ValueError, total_score_player_against_opponent_from_str_list, ' ')

  def test_ValueError_with_invalid_second_hand(self):
    self.assertRaises(ValueError, total_score_player_against_opponent_from_str_list, 'A W')

  def test_ValueError_with_invalid_first_hand(self):
    self.assertRaises(ValueError, total_score_player_against_opponent_from_str_list, 'D X')
