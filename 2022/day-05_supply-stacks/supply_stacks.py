import re
from typing import List, Tuple

PADDING_SPACE = ' '
STACK_TERMINATOR_LINE = ""
StackType = List[str]


class Move:
  def __init__(self, quantity, source_number, destination_number):
    self.quantity = int(quantity)
    self.source_index = int(source_number) - 1
    self.destination_index = int(destination_number) - 1


def top_crates_after_single_crate_moves(filename: str) -> str:
  """1 crate at a time for each multi-crates moves"""
  moves, stacks = parse_moves_and_stacks(filename)
  apply_single_crate_moves_to_stacks(moves, stacks)
  print_stacks_of_file(stacks, filename)
  return top_crates_str(stacks)


def top_crates_after_multi_crates_moves(filename: str) -> str:
  """all crates at once for each multi-crates moves"""
  moves, stacks = parse_moves_and_stacks(filename)
  apply_multi_crates_moves_to_stacks(moves, stacks)
  print_stacks_of_file(stacks, filename)
  return top_crates_str(stacks)


def parse_moves_and_stacks(filename: str) -> Tuple[List[Move], List[StackType]]:
  move_lines_str_list, stack_lines_str_list = read_stack_and_moves_lines(filename)
  stacks = parse_stacks_from_lines(stack_lines_str_list)
  moves = parse_moves_from_lines(move_lines_str_list)
  return moves, stacks


def apply_single_crate_moves_to_stacks(moves: List[Move], stacks: List[StackType]):
  for move in moves:
    # wayup: validate indices and quantity/source-stack-len
    src_stack = stacks[move.source_index]
    dst_stack = stacks[move.destination_index]
    if len(src_stack) > 1:  # the first element is the stack number
      for i in range(move.quantity):
        crate = src_stack.pop()
        dst_stack.append(crate)


def apply_multi_crates_moves_to_stacks(moves: List[Move], stacks: List[StackType]):
  for move in moves:
    # wayup: validate indices and quantity/source-stack-len
    src_stack = stacks[move.source_index]
    dst_stack = stacks[move.destination_index]
    if len(src_stack) > 1:  # the first element is the stack number
      mover_arm = []
      for i in range(move.quantity):
        crate = src_stack.pop()
        mover_arm.insert(0, crate)
      dst_stack.extend(mover_arm)


def read_stack_and_moves_lines(filename: str):
  # todo: handle files with moves by using the blank line under the stack numbers as a separator
  lines_str_list = read_lines(filename)
  stack_lines_str_list, move_lines_str_list = split_stacks_and_moves_lines(lines_str_list)
  return move_lines_str_list, stack_lines_str_list


def split_stacks_and_moves_lines(lines_str_list):
  stack_lines_str_list = []
  move_lines_str_list = []
  is_stack_line = True
  for line in lines_str_list:
    if line == STACK_TERMINATOR_LINE:
      is_stack_line = False
    elif is_stack_line:
      stack_lines_str_list.append(line)
    else:
      move_lines_str_list.append(line)
  return stack_lines_str_list, move_lines_str_list


def parse_stacks_from_lines(lines_str: List[str]) -> List[StackType]:
  def pad_lines(line_strings: List[str]) -> List[str]:
    max_line_len = max(map(len, line_strings))

    def pad_line(line: str) -> str:
      pad_spaces = PADDING_SPACE * (max_line_len - len(line))
      return line + pad_spaces

    padded_lines = list(map(pad_line, line_strings))
    return padded_lines

  def transpose_padded_lines_into_stacks(padded_lines: List[str]) -> List[StackType]:
    # Obtaining the stack column indices
    # wayup: handle more than 9 stacks which leads to a variable step because of the number of digits
    #  (However, the puzzle char_stream only has 9 stacks.)
    first_stack_column_index = 1  # Rather than 0 because of the space under [, although it might vary if the first stack is empty.
    stack_columns_step = 4
    max_line_len = max(map(len, padded_lines))
    stack_columns_indices = range(first_stack_column_index, max_line_len, stack_columns_step)

    # Each stack contains the stack number, then its bottom crate to its top crate (order obtained using `reversed()`)
    # The stacks are obtained by doing a transposition on the lines,
    # and by selecting the crates letter-representation among the other characters on each line.
    stacks: list = [[line[i] for line in reversed(padded_lines)]
                    for i in stack_columns_indices]
    return stacks

  def remove_remaining_top_padding(stacks: List[StackType]) -> None:
    # It mutates the argument.
    for s in stacks:
      while s[-1] == PADDING_SPACE:  # todo: add tests with more crates for requiring a while loop instead of an if
        s.pop()

  padded_lines = pad_lines(lines_str)
  result = transpose_padded_lines_into_stacks(padded_lines)
  remove_remaining_top_padding(result)
  return result


def parse_moves_from_lines(move_lines_str_list: List[str]) -> List[Move]:
  moves = []
  for move_line in move_lines_str_list:
    result = re.search("move (\\d+) from (\\d+) to (\\d+)", move_line)
    move = Move(result.group(1), result.group(2), result.group(3))
    moves.append(move)
  return moves


def print_stacks_of_file(stacks: List[StackType], filename: str) -> None:
  stacks_str = map(lambda x: " ".join(x), stacks)
  print('--- ' + filename)
  print("\n".join(stacks_str))  # wayup: replace \n by the system's line separator


# list type hint instead of List[StackType] to avoid a TBC false-positive warning, wayup: look into it
def top_crates_str(stacks: list) -> str:
  # wayup: handle stacks with no crate (they would only contain their number with the current impl)
  top_crates_list = map(list.pop, stacks)
  result = ''.join(top_crates_list)
  return result


def read_lines(filename: str) -> List[str]:
  with open(filename, 'r') as reader:
    lines_str = reader.read().splitlines()
  return lines_str


if __name__ == '__main__':
  single_crate_mover_answer = top_crates_after_single_crate_moves('./puzzle_input.txt')
  multi_crate_mover_answer = top_crates_after_multi_crates_moves('./puzzle_input.txt')
  print(f'Single-crate mover answer: {single_crate_mover_answer}')
  print(f'Multi-crates mover answer: {multi_crate_mover_answer}')
