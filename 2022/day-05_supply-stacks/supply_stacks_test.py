from unittest import TestCase, main
from supply_stacks import *


class SupplyStackTest(TestCase):
  def test_1_stack_1_crate_A_noMoves(self):
    self.assertEqual('A', top_crates_after_single_crate_moves('test_data/1-stack_1-crate-A_no-moves.txt'))

  def test_1_stack_1_crate_B_noMoves(self):
    self.assertEqual('B', top_crates_after_single_crate_moves('test_data/1-stack_1-crate-B_no-moves.txt'))

  def test_2_stacks_1_crateEach_CD_noMoves(self):
    self.assertEqual('CD', top_crates_after_single_crate_moves('test_data/2-stacks_1-crate-each_CD_no-moves.txt'))

  def test_2_stacks_2_and_1_crates_EF_noMoves(self):
    self.assertEqual('EF', top_crates_after_single_crate_moves('test_data/2-stacks_2-and-1-crates_EF_no-moves.txt'))

  def test_example_noMoves(self):
    self.assertEqual('NDP', top_crates_after_single_crate_moves('test_data/example_NDP_no-moves.txt'))

  def test_example_noMoves_blankLine(self):
    self.assertEqual('NDP', top_crates_after_single_crate_moves('test_data/example_NDP_no-moves_blank-line.txt'))

  def test_example_firstMove(self):
    self.assertEqual('DCP', top_crates_after_single_crate_moves('test_data/example_NDP-to-DCP_1-move.txt'))

  def test_fullExample(self):
    self.assertEqual('CMZ', top_crates_after_single_crate_moves('test_data/example_NDP-to-CMZ.txt'))

  def test_fullExample_globalMoves(self):
    self.assertEqual('MCD', top_crates_after_multi_crates_moves('test_data/example_NDP-to-CMZ.txt'))


if __name__ == '__main__':
  main()
