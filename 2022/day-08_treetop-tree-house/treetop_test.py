from unittest import TestCase, main
from treetop import *


# Supposition: the forest has the shape of a rectangle
class VisibleTreesFromOutsideTest(TestCase):
  def test_example(self):
    self.assertExpectedCountForForest(21, read_file_lines('example.txt'))

  def test_zero_withoutTree(self):
    self.assertExpectedCountForForest(0, [])
    self.assertExpectedCountForForest(0, [''])

  def test_visible_withSingleTree(self):
    self.assertExpectedCountForForest(1, ['5'])

  def test_allVisible_withOneRow(self):
    self.assertExpectedCountForForest(5, ['24378'])

  def test_allVisible_withTwoRows(self):
    self.assertExpectedCountForForest(6, [
      '510',
      '811',
    ])

  def test_allVisible_withOneColumn(self):
    self.assertExpectedCountForForest(4, [
      '5',
      '8',
      '2',
      '7',
    ])

  def test_allVisible_withTwoColumns(self):
    self.assertExpectedCountForForest(8, [
      '53',
      '87',
      '24',
      '71',
    ])

  def test_allVisible_withTallerInTheMiddle(self):
    self.assertExpectedCountForForest(9, [
      '444',
      '454',
      '444',
    ])

  def test_twoHiddenOfIncreasingSize_withTallerAround(self):
    self.assertExpectedCountForForest(13, [
      '44444',
      '45234',  # 3 is hidden by a distant tree to its left
      '44444',
    ])

  def test_allVisible_withVisibilityFromTheRight(self):
    self.assertExpectedCountForForest(12, [
      '4444',
      '4654',  # 5 is visible from its right
      '4444',
    ])

  def test_oneHidden_withShorterInTheMiddle(self):
    self.assertExpectedCountForForest(8, [
      '444',
      '434',
      '444',
    ])

  def test_oneHidden_withSameSizeWithinRow(self):
    self.assertExpectedCountForForest(8, [
      '555',
      '444',
      '555',
    ])

  def test_oneHidden_withSameSizeWithinColumn(self):
    self.assertExpectedCountForForest(8, [
      '656',
      '656',
      '656',
    ])

  def test_oneHidden_withAllSameSize(self):
    self.assertExpectedCountForForest(8, [
      '888',
      '888',
      '888',
    ])

  def test_allVisible_withMiddleVisibleFromBottom(self):
    self.assertExpectedCountForForest(9, [
      '888',
      '888',
      '878',
    ])

  def test_notVisibleInDiagonal_withSameSizeWithinColumn(self):
    self.assertExpectedCountForForest(8, [
      '020',
      '212',
      '020',
    ])

  def test_oneHidden_withShorterInTheMiddleOf9(self):
    self.assertExpectedCountForForest(8, [
      '553',
      '807',
      '234',
    ])

  def test_oneHidden_withShorterInTheMiddleOf12Wider(self):
    self.assertExpectedCountForForest(11, [
      '5523',
      '8417',
      '2324',
    ])

  def test_twoHidden_withShorterInTheMiddleOf12Taller(self):
    self.assertExpectedCountForForest(10, [
      '552',
      '801',
      '444',
      '252',
    ])

  def test_oneHidden_withShorterInTheMiddleOf12Taller(self):
    self.assertExpectedCountForForest(11, [
      '552',
      '891',
      '444',
      '252',
    ])

  def test_twoHidden_with5by5forest(self):
    self.assertExpectedCountForForest(23, [
      '55213',
      '89164',  # 1 is hidden
      '45634',  # 3 is hidden
      '27755',  # the left 5 is visible from below
      '15415',
    ])

  def test_threeHidden_with5by5forest(self):
    self.assertExpectedCountForForest(22, [
      '55213',
      '89164',  # 1 is hidden
      '45634',  # 3 is hidden
      '27755',  # the left 5 is hidden
      '15455',
    ])

  def assertExpectedCountForForest(self, expected: int, forest_rows: List[str]):
    result = count_trees_visible_from_outside(forest_rows)
    self.assertEqual(expected, result)


TreePosType = Tuple[int, int]


def transpose(forest_rows):
  return [''.join(chars_of_transposed_row) for chars_of_transposed_row in zip(*forest_rows)]


class ViewingDistanceTest(TestCase):
  def test_emptyForest(self):
    self.assertScenicScoreZeroFromAnyTree([])
    self.assertScenicScoreZeroFromAnyTree([''])

  def test_singleRowForest(self):
    self.assertScenicScoreZeroFromAnyTree(['1'])
    self.assertScenicScoreZeroFromAnyTree(['12'])
    self.assertScenicScoreZeroFromAnyTree(['123'])

  def test_singleColumnForest(self):
    self.assertScenicScoreZeroFromAnyTree(['1'])

    self.assertScenicScoreZeroFromAnyTree(['1',
                                           '2'])

    self.assertScenicScoreZeroFromAnyTree(['1',
                                           '2',
                                           '3'])

  def test_twoRowsForest(self):
    self.assertScenicScoreZeroFromAnyTree(['1',
                                           '2'])
    self.assertScenicScoreZeroFromAnyTree(['12',
                                           '34'])
    self.assertScenicScoreZeroFromAnyTree(['123',
                                           '456'])

  def test_twoColumnsForest(self):
    self.assertScenicScoreZeroFromAnyTree(['12'])
    self.assertScenicScoreZeroFromAnyTree(['12',
                                           '34'])
    self.assertScenicScoreZeroFromAnyTree(['12',
                                           '34',
                                           '56'])

  def test_tallerTreeInTheMiddle(self):
    self.forest_rows = ['444',
                        '454',
                        '444']
    middle_tree = (1, 1)

    self.assertScenicScoreFromTree(1, middle_tree, self.forest_rows)
    self.assertScenicScoreFromAllTreesExcept(0, [middle_tree], self.forest_rows)

  def test_tallerTreesInTheMiddleOf4Columns(self):
    forest_rows = ['4444',
                   '4554',
                   '4444']
    middle_trees = [(1, 1), (1, 2)]

    self.assertScenicScoreFromTrees(1, middle_trees, forest_rows)
    self.assertScenicScoreFromAllTreesExcept(0, middle_trees, forest_rows)

  def test_tallerTreesInTheMiddleOf4Rows(self):
    forest_rows = ['444',
                   '454',
                   '454',
                   '444']
    middle_trees = [(1, 1), (2, 1)]

    self.assertScenicScoreFromTrees(1, middle_trees, forest_rows)
    self.assertScenicScoreFromAllTreesExcept(0, middle_trees, forest_rows)

  def test_oneVisibleTreeOnEachSideFromTheMiddle_singleSameSizeNeighbor(self):
    forest_rows = ['444',
                   '554',
                   '444']
    middle_tree = (1, 1)
    self.assertScenicScoreFromTrees(1, [middle_tree], forest_rows)
    self.assertScenicScoreFromAllTreesExcept(0, [middle_tree], forest_rows)

  def test_oneVisibleTreeOnEachSideFromTheMiddle_sameSizeNeighbors(self):
    forest_rows = ['454',
                   '555',
                   '454']
    middle_tree = (1, 1)
    self.assertScenicScoreFromTrees(1, [middle_tree], forest_rows)
    self.assertScenicScoreFromAllTreesExcept(0, [middle_tree], forest_rows)

  def test_oneVisibleTreeOnEachSideFromTheMiddle_tallerNeighbors(self):
    forest_rows = ['464',
                   '656',
                   '464']
    middle_tree = (1, 1)
    self.assertScenicScoreFromTrees(1, [middle_tree], forest_rows)
    self.assertScenicScoreFromAllTreesExcept(0, [middle_tree], forest_rows)

  def test_scenicScoreTwo_fromMiddleLeft_6544(self):
    self.subtest_scenicScoreOfTwoHorizontalMiddleTrees(2, 1, ['4664',
                                                              '6544',
                                                              '4664'])

  def test_scenicScoreTwo_fromMiddleLeft_6545(self):
    self.subtest_scenicScoreOfTwoHorizontalMiddleTrees(2, 1, ['4664',
                                                              '6545',
                                                              '4664'])

  def test_scenicScoreTwo_fromMiddleLeft_6546(self):
    self.subtest_scenicScoreOfTwoHorizontalMiddleTrees(2, 1, ['4664',
                                                              '6546',
                                                              '4664'])

  def test_scenicScoreOne_fromHorizontalMiddleTrees_6556(self):
    self.subtest_scenicScoreOfTwoHorizontalMiddleTrees(1, 1, ['4664',
                                                              '6556',
                                                              '4664'])

  def test_scenicScoreTwo_fromMiddleRight_6456(self):
    self.subtest_scenicScoreOfTwoHorizontalMiddleTrees(1, 2, ['4664',
                                                              '6456',
                                                              '4664'])

  def test_scenicScoreTwo_fromMiddleRight_4456(self):
    self.subtest_scenicScoreOfTwoHorizontalMiddleTrees(1, 2, ['4664',
                                                              '4456',
                                                              '4664'])

  def test_scenicScoreTwo_fromMiddleRight_5456(self):
    self.subtest_scenicScoreOfTwoHorizontalMiddleTrees(1, 2, ['4664',
                                                              '5456',
                                                              '4664'])

  def subtest_scenicScoreOfTwoHorizontalMiddleTrees(self,
                                                    expected_middle_left: int,
                                                    expected_middle_right: int,
                                                    forest_rows: List[str]):
    """It only makes sense to call this with a forest with 3 rows and 4 columns at the moment."""
    assert_rectangular_forest(forest_rows)
    assert len(forest_rows) == 3
    assert len(forest_rows[0]) == 4

    middle_left_tree = (1, 1)
    middle_right_tree = (1, 2)
    middle_trees = [middle_left_tree, middle_right_tree]

    self.assertScenicScoreFromAllTreesExcept(0, middle_trees, forest_rows)
    self.assertScenicScoreFromTrees(expected_middle_left, [middle_left_tree], forest_rows)
    self.assertScenicScoreFromTrees(expected_middle_right, [middle_right_tree], forest_rows)

  def test_scenicScoreTwo_fromMiddleTop_6544(self):
    self.subtest_scenicScoreOfTwoVerticalMiddleTrees(2, 1, transpose(['4664',
                                                                      '6544',
                                                                      '4664']))

  def test_scenicScoreTwo_fromMiddleTop_6545(self):
    self.subtest_scenicScoreOfTwoVerticalMiddleTrees(2, 1, transpose(['4664',
                                                                      '6545',
                                                                      '4664']))

  def test_scenicScoreTwo_fromMiddleTop_6546(self):
    self.subtest_scenicScoreOfTwoVerticalMiddleTrees(2, 1, transpose(['4664',
                                                                      '6546',
                                                                      '4664']))

  def test_scenicScoreOne_fromVerticalMiddleTrees_6556(self):
    self.subtest_scenicScoreOfTwoVerticalMiddleTrees(1, 1, transpose(['4664',
                                                                      '6556',
                                                                      '4664']))

  def test_scenicScoreTwo_fromMiddleBottom_6456(self):
    self.subtest_scenicScoreOfTwoVerticalMiddleTrees(1, 2, transpose(['4664',
                                                                      '6456',
                                                                      '4664']))

  def test_scenicScoreTwo_fromMiddleBottom_4456(self):
    self.subtest_scenicScoreOfTwoVerticalMiddleTrees(1, 2, transpose(['4664',
                                                                      '4456',
                                                                      '4664']))

  def test_scenicScoreTwo_fromMiddleBottom_5456(self):
    self.subtest_scenicScoreOfTwoVerticalMiddleTrees(1, 2, transpose(['4664',
                                                                      '5456',
                                                                      '4664']))

  def subtest_scenicScoreOfTwoVerticalMiddleTrees(self,
                                                  expected_middle_top: int,
                                                  expected_middle_bottom: int,
                                                  forest_rows: List[str]):
    """It only makes sense to call this with a forest with 4 rows and 3 columns at the moment."""
    assert_rectangular_forest(forest_rows)
    assert len(forest_rows) == 4
    assert len(forest_rows[0]) == 3

    middle_top_tree = (1, 1)
    middle_bottom_tree = (2, 1)
    middle_trees = [middle_top_tree, middle_bottom_tree]

    self.assertScenicScoreFromAllTreesExcept(0, middle_trees, forest_rows)
    self.assertScenicScoreFromTrees(expected_middle_top, [middle_top_tree], forest_rows)
    self.assertScenicScoreFromTrees(expected_middle_bottom, [middle_bottom_tree], forest_rows)

  def test_example(self):
    forest_rows = ['30373',
                   '25512',
                   '65332',
                   '33549',
                   '35390']
    self.assertScenicScoreFromTree(4, (1, 2), forest_rows)
    self.assertScenicScoreFromTree(8, (3, 2), forest_rows)

  def assertScenicScoreZeroFromAnyTree(self, forest_rows: List[str]):
    self.assertScenicScoreFromAllTreesExcept(0, [], forest_rows)

  def assertScenicScoreFromAllTreesExcept(self,
                                          expected: int,
                                          except_trees: List[TreePosType],
                                          forest_rows: List[str]):
    for row in range(len(forest_rows)):
      for col in range(len(forest_rows[row])):
        if (row, col) not in except_trees:
          self.assertScenicScoreFromTree(expected, (row, col), forest_rows)

  def assertScenicScoreFromTrees(self,
                                 expected: int,
                                 trees: List[TreePosType],
                                 forest_rows: List[str]):
    for tree in trees:
      self.assertScenicScoreFromTree(expected, tree, forest_rows)

  def assertScenicScoreFromTree(self,
                                expected: int,
                                tree_pos: TreePosType,
                                forest_rows: List[str]):
    """The tree position is represented as (row, col) and zero-based."""
    result = calculate_scenic_score_at(tree_pos, forest_rows)
    self.assertEqual(expected, result,
                     f'The tree of size {forest_rows[tree_pos[0]][tree_pos[1]]} at {tree_pos} zero-based '
                     f'in the following forest should have scenic score {expected}, but got {result}:' +
                     os.linesep + os.linesep.join(forest_rows) + os.linesep)


class MaximumScenicScore(TestCase):
  def test_example(self):
    forest_rows = ['30373',
                   '25512',
                   '65332',
                   '33549',
                   '35390']
    self.assertMaximumScenicScore(8, forest_rows)

  def assertMaximumScenicScore(self, expected: int, forest_rows: List[str]):
    result = maximum_scenic_score_in(forest_rows)
    self.assertEqual(expected, result,
                     f'The maximum scenic score in the following forest should be {expected}, but got {result}:' +
                     os.linesep + os.linesep.join(forest_rows) + os.linesep)


class AssertRectangularForest(TestCase):
  def test_rectangular(self):
    self.assertRectangular(['123',
                            '456',
                            '789'])

  def test_notRectangular_rowTooShort(self):
    with self.assertRaises(AssertionError):
      self.assertRectangular(['123',
                              '456',
                              '78'])

  def test_notRectangular_rowTooLong(self):
    with self.assertRaises(AssertionError):
      self.assertRectangular(['123',
                              '456',
                              '7890'])

  def assertRectangular(self, forest_rows: List[str]):
    assert_rectangular_forest(forest_rows)


if __name__ == '__main__':
  main()
