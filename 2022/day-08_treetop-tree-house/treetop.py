import os
from typing import List, Callable, Tuple


def count_trees_visible_from_outside(forest_rows: List[str]) -> int:
  print(os.linesep +
        'Forest:' + os.linesep +
        os.linesep.join(forest_rows))

  forest = parse_rectangular_forest_to_ints(forest_rows)
  row_count = len(forest)
  col_count = len(forest[0]) if row_count > 0 else 0

  visible_count = 0

  # The tree are identified to their size,
  # i.e. the tree variable and arrays actual contain the size of the trees.
  for r in range(row_count):
    for c in range(col_count):
      tree = forest[r][c]

      # The conditional assignments avoid calling max() on an empty list
      # (which would make it throw an exception).

      left_trees = [forest[r][i] for i in range(c)]
      left_max = max(left_trees) if left_trees != [] else -1

      right_trees = [forest[r][i] for i in range(c + 1, col_count)]
      right_max = max(right_trees) if right_trees != [] else -1

      top_trees = [forest[i][c] for i in range(r)]
      top_max = max(top_trees) if top_trees != [] else -1

      bottom_trees = [forest[i][c] for i in range(r + 1, row_count)]
      bottom_max = max(bottom_trees) if bottom_trees != [] else -1

      visible = tree > left_max or \
                tree > right_max or \
                tree > top_max or \
                tree > bottom_max

      if visible:
        visible_count += 1
      else:
        # print(f'- hidden tree at [{r + 1}, {c + 1}]')  # +1 for 1-base indices  # debug output
        pass

  return visible_count


def maximum_scenic_score_in(forest: List[str]) -> int:
  """
  Returns the maximum scenic score in the given forest.

  The forest is represented as a list of strings, where each string is a row of the forest.
  Each number in the string is a tree of that size.
  """
  assert_rectangular_forest(forest)

  return max([
    calculate_scenic_score_at((row, col), forest)
    for row in range(len(forest))
    for col in range(len(forest[row]))
  ])


def calculate_scenic_score_at(tree_position: Tuple[int, int],
                              forest: List[str]) -> int:
  """
  :param tree_position: the position of the tree in the forest, zero-based, (0, 0) is the top-left tree
  :param forest: the forest, represented as a list of strings, where each string is a row of the forest
  """

  assert_rectangular_forest(forest)

  tree_row, tree_col = tree_position
  given_tree = forest[tree_row][tree_col]
  row_count = len(forest)
  col_count = len(forest[0]) if row_count > 0 else 0

  top, left, bottom, right = range(4)  # directions

  def visibility_to_the(direction: int) -> int:
    """
    :param direction: the direction to check, one of the constants top, left, bottom, right
    :return: the number of visible trees in the given direction from the given tree position
    """
    visibility = 0
    for other_tree in trees_to_the(direction):
      visibility += 1
      if other_tree >= given_tree:  # The tree is at least as tall as the given tree, so it blocks the view.
        break
    return visibility

  def trees_to_the(direction: int) -> List[str]:
    """
    :param direction:  the direction to look in, one of the constants top, left, bottom, right
    :return: the list of trees in the given direction from the given tree position (excluded),
             ordered from the closest to the farthest tree
    """
    if direction == right:
      return [forest[tree_row][c] for c in range(tree_col + 1, col_count, 1)]
    elif direction == left:
      return [forest[tree_row][c] for c in range(tree_col - 1, -1, -1)]
    elif direction == bottom:
      return [forest[r][tree_col] for r in range(tree_row + 1, row_count, 1)]
    elif direction == top:
      return [forest[r][tree_col] for r in range(tree_row - 1, -1, -1)]
    else:
      raise ValueError(f'Invalid direction: {direction}')

  return visibility_to_the(right) * visibility_to_the(left) * visibility_to_the(bottom) * visibility_to_the(top)


# wayup: add tests
def assert_rectangular_forest(forest: List[str]) -> None:
  """
  Asserts that the given forest is rectangular, i.e. all rows have the same length.
  :param forest: the forest, represented as a list of strings, where each string is a row of the forest
  """
  row_count = len(forest)
  col_count = len(forest[0]) if row_count > 0 else 0

  for r in range(row_count):
    assert len(forest[r]) == col_count, \
      f'forest row {r} has {len(forest[r])} columns, expected {col_count}'


def parse_rectangular_forest_to_ints(forest_rows: List[str]):
  assert_rectangular_forest(forest_rows)
  forest: List[List[int]] = []
  for row in forest_rows:
    chars = list(row)
    forest.append(list(map(int, chars)))
  return forest


def read_file_lines(file: str) -> List[str]:
  with open(file, 'r') as reader:
    return reader.read().splitlines()


if __name__ == '__main__':
  visible_trees_from_outside = count_trees_visible_from_outside(read_file_lines('./puzzle-input.txt'))
  print(f'{visible_trees_from_outside=}')

  maximum_scenic_score = maximum_scenic_score_in(read_file_lines('./puzzle-input.txt'))
  print(f'{maximum_scenic_score=}')
