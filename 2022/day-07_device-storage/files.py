import os
from typing import List, cast


class Node:
  def __init__(self, name: str):
    self.name = name
    self.parent = None

  def total_size(self) -> int:
    raise NotImplementedError


class Directory(Node):
  def __init__(self, name: str, *children: 'Node'):
    super().__init__(name)
    self.children: List[Node] = []
    self.add_children(*children)

  def __repr__(self) -> str:
    result = ''
    if self.parent is None:
      result += os.linesep + 'Directory tree:' + os.linesep
    result += self.name
    for child in self.children:
      result += os.linesep
      result += '  ' + child.__repr__()
    return result

  def __eq__(self, other: object) -> bool:
    # Check for type equality and cast if so
    if type(other) != Directory:
      return False
    else:
      other = cast(Directory, other)

    if self.name != other.name:
      return False

    if len(self.children) != len(other.children):
      return False

    # In this equality implementation, the ordering of the children
    # should be the same for the directories to be considered equal.
    for i in range(len(self.children)):
      if self.children[i] != other.children[i]:
        return False

    return True

  def total_size(self) -> int:
    return sum(map(lambda c: c.total_size(), self.children))

  # wayup: do various checks with respect to file system's logic
  def add_children(self, *children: 'Node') -> None:
    for child in children:
      child.parent = self
      self.children.append(child)

  def find_child_by_name(self, name: str) -> Node:
    for child in self.children:
      if child.name == name:
        return child
    raise ValueError(f'Child not found: {name}')

  def subdirs(self) -> List['Directory']:
    return list(filter(lambda c: type(c) == Directory, self.children))


class File(Node):
  def __init__(self, name: str, size: int):
    super().__init__(name)
    self.size = size

  def __repr__(self) -> str:
    return f'{self.name} ({self.size})'

  def __eq__(self, other: object) -> bool:
    """
    The files are not defined in terms of content,
    therefore the equality check only looks at the names and the sizes.
     """
    if type(other) == File:
      other_file = cast(File, other)
      return self.name == other_file.name and \
        self.size == other_file.size
    else:
      return False

  def total_size(self) -> int:
    return self.size
