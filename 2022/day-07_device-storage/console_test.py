from unittest import TestCase, main
from console import *
from files import Directory, File
from example_object import EXAMPLE_OBJECT


class FileTreeParser(TestCase):
  def test_singleFileInRoot(self):
    expected = Directory('/', File('file.txt', 10))
    result = parse_directory_from_console_trace([
      '$ cd /',
      '$ ls',
      '10 file.txt',
    ])
    self.assertEqual(expected, result)

  def test_singleFileInRoot_withEmptyLineInBetween(self):
    expected = Directory('/', File('file.txt', 10))
    result = parse_directory_from_console_trace([
      '$ cd /',
      '',
      '$ ls',
      '10 file.txt',
    ])
    self.assertEqual(expected, result)

  def test_singleOtherFileInRoot(self):
    expected = Directory('/', File('other_file.txt', 20))
    result = parse_directory_from_console_trace([
      '$ cd /',
      '$ ls',
      '20 other_file.txt',
    ])
    self.assertEqual(expected, result)

  def test_twoFilesInRoot(self):
    expected = Directory('/',
                         File('file_1.txt', 30),
                         File('file_2.txt', 40))
    result = parse_directory_from_console_trace([
      '$ cd /',
      '$ ls',
      '30 file_1.txt',
      '40 file_2.txt',
    ])
    self.assertEqual(expected, result)

  def test_singleSubDirInRoot(self):
    expected = Directory('/', Directory('subdir'))
    result = parse_directory_from_console_trace([
      '$ cd /',
      '$ ls',
      'dir subdir',
    ])
    self.assertEqual(expected, result)

  def test_threeSubDirInRoot(self):
    expected = Directory('/',
                         Directory('subdir_1'),
                         Directory('subdir_2'),
                         Directory('subdir_3'))
    result = parse_directory_from_console_trace([
      '$ cd /',
      '$ ls',
      'dir subdir_1',
      'dir subdir_2',
      'dir subdir_3',
    ])
    self.assertEqual(expected, result)

  def test_fileAndSubDirsInRoot(self):
    expected = Directory('/',
                         Directory('subdir_1'),
                         File('file', 100),
                         Directory('subdir_2'))
    result = parse_directory_from_console_trace([
      '$ cd /',
      '$ ls',
      'dir subdir_1',
      '100 file',
      'dir subdir_2',
    ])
    self.assertEqual(expected, result)

  def test_fileNestedInSubDirectory(self):
    expected = Directory('/',
                         Directory('subdir', File('nested_file', 200)))
    result = parse_directory_from_console_trace([
      '$ cd /',
      '$ ls',
      'dir subdir',
      '$ cd subdir',
      '$ ls',
      '200 nested_file',
    ])
    self.assertEqual(expected, result)

  def test_cdToParent(self):
    expected = Directory('/',
                         Directory('subdir_1', File('nested_file_1', 100)),
                         Directory('subdir_2', File('nested_file_2', 200)))
    result = parse_directory_from_console_trace([
      '$ cd /',
      '$ ls',
      'dir subdir_1',
      'dir subdir_2',
      '$ cd subdir_1',
      '$ ls',
      '100 nested_file_1',
      '$ cd ..',
      '$ cd subdir_2',
      '$ ls',
      '200 nested_file_2',
    ])
    self.assertEqual(expected, result)

  def test_example_parsing(self):
    with open('example.txt', 'r') as reader:
      example_lines = reader.read().splitlines()
    result = parse_directory_from_console_trace(example_lines)
    self.assertEqual(EXAMPLE_OBJECT, result)


if __name__ == '__main__':
  main()
