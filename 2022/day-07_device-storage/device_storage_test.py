from unittest import TestCase, main
from device_storage import *


class DeviceStorageTest(TestCase):
  def test_returns0_withEmptyTrace(self):
    self.assert_expected_sum(0, [])

  def test_returns0_withSingleEmptyLine(self):
    self.assert_expected_sum(0, [''])

  def test_returns10_withSingleFileInRoot(self):
    self.assert_expected_sum(10, [
      '$ cd /',
      '$ ls',
      '10 file.txt',
    ])

  def test_returns20_withTwoFilesInRoot(self):
    self.assert_expected_sum(20, [
      '$ cd /',
      '$ ls',
      '10 file1.txt',
      '10 file2.txt',
    ])

  def test_returns30_withTwoFilesInRoot(self):
    self.assert_expected_sum(30, [
      '$ cd /',
      '$ ls',
      '10 file1.txt',
      '20 file2.txt',
    ])

  def test_returns0_withTooBigFileInRoot(self):
    # The root directory should be excluded because its size exceeds the limit of 100000.
    self.assert_expected_sum(0, [
      '$ cd /',
      '$ ls',
      '10 file.txt',
      '100100100 file.txt',
    ])

  def test_returns60_withNestedFile(self):
    # 60 since the total size of / (30) is counted
    # in addition to the total size of its subdir (30).
    self.assert_expected_sum(60, [
      '$ cd /',
      '$ ls',
      'dir subdir',
      '$ cd subdir',
      '$ ls',
      '30 file.txt',
    ])

  def test_returns30_withNestedFile_withFilesExceedingTheLimitInRoot(self):
    # 60 since the total size of / (30) is counted
    # in addition to the total size of its subdir (30).
    self.assert_expected_sum(30, [
      '$ cd /',
      '$ ls',
      'dir subdir',
      '100123 file_bigger_than_the_limit.txt',
      '$ cd subdir',
      '$ ls',
      '30 file.txt',
    ])

  def test_returns30_withDoublyNestedFile_withFilesExceedingTheLimitAbove(self):
    # 60 since the total size of / (30) is counted
    # in addition to the total size of its subdir (30).
    self.assert_expected_sum(40, [
      '$ cd /',
      '$ ls',
      'dir subdir1',
      '100123 file_bigger_than_the_limit.txt',
      '$ cd subdir1',
      '$ ls',
      'dir subdir2',
      '100123 file_bigger_than_the_limit.txt',
      '$ cd subdir2',
      '$ ls',
      '40 file.txt',
    ])

  def test_returns90_withDirectAndNestedFiles(self):
    self.assert_expected_sum(90, [
      '$ cd /',
      '$ ls',
      '10 file1.txt',
      '20 file2.txt',
      'dir subdir',
      '$ cd subdir',
      '$ ls',
      '30 file.txt',
    ])

  def assert_expected_sum(self, expected: int, trace: List[str]):
    self.assertEqual(expected, sum_directory_sizes_up_to_threshold_from_trace(trace))

  def test_example_parsing(self):
    example_lines = read_example_trace()
    result = sum_directory_sizes_up_to_threshold_from_trace(example_lines)
    self.assertEqual(95437, result)


class FindDirectoryToFreeUpSpaceTest(TestCase):
  def test_usedSpace_withExampleTxt(self):
    self.assertEqual(48381165, used_space(read_example_trace()))

  def test_availableSpace_withExampleTxt(self):
    self.assertEqual(21618835, available_space(read_example_trace()))

  def test_missingSpace_withExampleTxt(self):
    self.assertEqual(8381165, missing_space_for_update(read_example_trace()))

  def test_dirsBiggerThanMissingSpace_withExampleTxt(self):
    candidates = candidate_dirs_to_increase_available_space_for_update(read_example_trace())
    sizes = list(sorted(map(lambda x: x, candidates)))
    expected_sizes = [24933642, 48381165]
    self.assertEqual(expected_sizes, sizes)

  def test_smallestCandidateSize_withExampleTxt(self):
    size = size_of_smallest_candidate_dir_to_increase_available_space_for_update(read_example_trace())
    self.assertEqual(24933642, size)


def read_example_trace():
  with open('example.txt', 'r') as reader:
    example_lines = reader.read().splitlines()
  return example_lines


if __name__ == '__main__':
  main()
