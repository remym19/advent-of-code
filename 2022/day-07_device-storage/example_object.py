from files import Directory, File


def make_example_object():
  root = Directory('/')
  a = Directory('a')
  b = File('b.txt', 14848514)
  c = File('c.dat', 8504156)
  d = Directory('d')
  root.add_children(a, b, c, d)

  e = Directory('e')
  f = File('f', 29116)
  g = File('g', 2557)
  h = File('h.lst', 62596)
  a.add_children(e, f, g, h)

  i = File('i', 584)
  e.add_children(i)

  j = File('j', 4060174)
  d_log = File('d.log', 8033020)
  d_ext = File('d.ext', 5626152)
  k = File('k', 7214296)
  d.add_children(j, d_log, d_ext, k)

  return root


EXAMPLE_OBJECT = make_example_object()
