from unittest import TestCase, main
from files import Directory, File
from example_object import EXAMPLE_OBJECT


class DirectoryTotalSizeTest(TestCase):
  def test_fileTotalSize_isFileSize(self):
    file = File('f', 1)
    self.assertEqual(1, file.total_size())

  def test_dirTotalSize_isFileSize(self):
    directory = Directory('d')
    file = File('f', 2)
    directory.add_children(file)
    self.assertEqual(2, directory.total_size())

  def test_dirTotalSize_isNestedFileSize(self):
    d1 = Directory('d1')
    d2 = Directory('d2')
    f = File('f', 3)
    d1.add_children(d2)
    d2.add_children(f)
    self.assertEqual(3, d1.total_size())

  def test_exampleTotalSize(self):
    self.assertEqual(48381165, EXAMPLE_OBJECT.total_size())


class FileEqualityTest(TestCase):
  def test_reflexivity(self):
    f = File('any', 123)
    self.assertEqual(f, f)

  def test_equal_withSameNameAndSize(self):
    x = File('a', 123)
    y = File('a', 123)
    self.assertTwoWayEqual(x, y)

  def test_notEqual_withSameSizeButDifferentNames(self):
    x = File('a', 123)
    y = File('other', 123)
    self.assertTwoWayNotEqual(x, y)

  def test_notEqual_withSameNameButDifferentSizes(self):
    x = File('a', 123)
    y = File('a', 456)
    self.assertTwoWayNotEqual(x, y)

  def assertTwoWayEqual(self, first, second):
    """Helper assertion to test check the symetry of equality in the test scenarios."""
    self.assertEqual(first, second)
    self.assertEqual(second, first)

  def assertTwoWayNotEqual(self, first, second):
    """Helper assertion to test check the symetry of non-equality in the test scenarios."""
    self.assertNotEqual(first, second)
    self.assertNotEqual(second, first)


class DirectoryEqualityTest(TestCase):
  def test_reflexivity(self):
    f = Directory('any')
    self.assertEqual(f, f)

  def test_notEqual_withDifferentNames(self):
    x = Directory('a')
    y = Directory('other')
    self.assertTwoWayNotEqual(x, y)

  def test_equal_withSameName_withNoChildAndSingleChild(self):
    x = Directory('same name', File('child in this directory only', 10))
    y = Directory('same name')
    self.assertTwoWayNotEqual(x, y)

  def test_equal_withSingleChild(self):
    x = Directory('same dir name', File('same child name', 20))
    y = Directory('same dir name', File('same child name', 20))
    self.assertTwoWayEqual(x, y)

  def test_Notequal_withSingleChildSameNameButDifferentSizes(self):
    x = Directory('same dir name', File('same child name', 30))
    y = Directory('same dir name', File('same child name', 1111))
    self.assertTwoWayNotEqual(x, y)

  def test_equal_withTwoChildren(self):
    x = Directory('same dir name',
                  File('same child name 1', 40),
                  File('same child name 2', 55))
    y = Directory('same dir name',
                  File('same child name 1', 40),
                  File('same child name 2', 55))
    self.assertTwoWayEqual(x, y)

  def test_notEqual_withTwoChildrenNotInTheSameOrder(self):
    x = Directory('same dir name',
                  File('same child name 1', 40),
                  File('same child name 2', 55))
    y = Directory('same dir name',
                  File('same child name 2', 55),
                  File('same child name 1', 40))
    self.assertTwoWayNotEqual(x, y)

  def test_equal_withNestedChildrenEqual(self):
    x = Directory('top dir',
                  File('child 1', 40),
                  File('child 2', 55),
                  Directory('sub dir',
                            File('nested child 1', 40),
                            File('nested child 2', 55)))
    y = Directory('top dir',
                  File('child 1', 40),
                  File('child 2', 55),
                  Directory('sub dir',
                            File('nested child 1', 40),
                            File('nested child 2', 55)))
    self.assertTwoWayEqual(x, y)

  def test_notEqual_withNestedChildrenSingleNameDifference(self):
    x = Directory('top dir',
                  File('child 1', 40),
                  File('child 2', 55),
                  Directory('sub dir',
                            File('nested child 1', 40),
                            File('nested child 2', 55)))
    y = Directory('top dir',
                  File('child 1', 40),
                  File('child 2', 55),
                  Directory('sub dir',
                            File('nested child 1', 40),
                            File('nested child _', 55)))
    self.assertTwoWayNotEqual(x, y)

  def test_notEqual_withNestedChildrenSingleSizeDifference(self):
    x = Directory('top dir',
                  File('child 1', 40),
                  File('child 2', 55),
                  Directory('sub dir',
                            File('nested child 1', 40),
                            File('nested child 2', 55)))
    y = Directory('top dir',
                  File('child 1', 40),
                  File('child 2', 55),
                  Directory('sub dir',
                            File('nested child 1', 40),
                            File('nested child 2', 2222)))
    self.assertTwoWayNotEqual(x, y)

  def assertTwoWayEqual(self, first, second):
    """Helper assertion to test check the symetry of equality in the test scenarios."""
    self.assertEqual(first, second)
    self.assertEqual(second, first)

  def assertTwoWayNotEqual(self, first, second):
    """Helper assertion to test check the symetry of non-equality in the test scenarios."""
    self.assertNotEqual(first, second)
    self.assertNotEqual(second, first)


class ChildSearchTest(TestCase):
  def test_findExistingFileChild(self):
    child = File('f', 1)
    directory = Directory('d', child)
    result = directory.find_child_by_name('f')
    self.assertEqual(child, result)

  def test_findExistingDirChild(self):
    child = Directory('sub')
    directory = Directory('d', child)
    result = directory.find_child_by_name('sub')
    self.assertEqual(child, result)

  def test_raiseError_withoutChild(self):
    directory = Directory('d')
    self.assertRaises(ValueError, directory.find_child_by_name, 'any')

  def test_raiseError_withChildOtherName(self):
    child = Directory('name')
    directory = Directory('d', child)
    self.assertRaises(ValueError, directory.find_child_by_name, 'other_name')

  def test_doesNotSearchForNestedChild_raiseErrorInstead(self):
    nested_child = Directory('nested')
    direct_child = Directory('direct', nested_child)
    directory = Directory('d', direct_child)
    self.assertRaises(ValueError, directory.find_child_by_name, 'nested')


class GetSubdirsTest(TestCase):
  def test_empty_withoutSubdirs(self):
    d = Directory('d')
    self.assertEqual([], d.subdirs())

  def test_empty_withChildFile_withoutSubdir(self):
    d = Directory('d', File('child', 10))
    self.assertEqual([], d.subdirs())

  def test_singleSubdir_withoutChildFile(self):
    sub = Directory('sub')
    d = Directory('d', sub)
    self.assertEqual([sub], d.subdirs())

  def test_singleSubdir_withChildFileAfterSubdir(self):
    subdir = Directory('sub')
    file = File('f', 20)
    d = Directory('d', subdir, file)
    self.assertEqual([subdir], d.subdirs())

  def test_singleSubdir_withChildFileBeforeSubdir(self):
    subdir = Directory('sub')
    file = File('f', 20)
    d = Directory('d', file, subdir)
    self.assertEqual([subdir], d.subdirs())

  def test_onlyDirectSubdir_withNestedSubdir(self):
    nested = Directory('nested')
    sub = Directory('sub', nested)
    d = Directory('d', sub)
    self.assertEqual([sub], d.subdirs())


if __name__ == '__main__':
  main()
