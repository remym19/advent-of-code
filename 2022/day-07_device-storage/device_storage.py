from typing import List, Callable, Tuple
from console import parse_directory_from_console_trace
from files import Directory

DIR_SIZE_SEARCH_MAX_THRESHOLD = 100 * 1000
DISK_SIZE = 70 * 1000 * 1000
UPDATE_SIZE = 30 * 1000 * 1000


def sum_directory_sizes_up_to_threshold_from_file(file: str) -> int:
  lines = read_lines(file)
  return sum_directory_sizes_up_to_threshold_from_trace(lines)


def sum_directory_sizes_up_to_threshold_from_trace(console_lines: List[str]) -> int:
  def upToThreshold_totalSizePredicate(d: Directory) -> bool:
    return d.total_size() <= DIR_SIZE_SEARCH_MAX_THRESHOLD

  directory = parse_directory_from_console_trace(console_lines)
  selection = find_subdirs_matching_predicate(directory, upToThreshold_totalSizePredicate)
  alternative_result = sum(map(lambda d: d.total_size(), selection))
  return alternative_result


def find_subdirs_matching_predicate(directory: Directory,
                                    directory_predicate: Callable[[Directory], bool]) -> List[Directory]:
  """Recursive depth-first-search"""

  selection: List[Directory] = []

  if directory_predicate(directory):
    selection.append(directory)

  for subdir in directory.subdirs():
    # Recursive call
    # wayup: replace by tail recursion with an accumulator if effective in Python
    sub_selection = find_subdirs_matching_predicate(subdir, directory_predicate)
    selection.extend(sub_selection)

  return selection


def used_space(console_lines: List[str]) -> int:
  directory = parse_directory_from_console_trace(console_lines)
  return directory.total_size()


def available_space(console_lines: List[str]) -> int:
  return DISK_SIZE - used_space(console_lines)


def missing_space_for_update(console_lines: List[str]) -> int:
  return UPDATE_SIZE - available_space(console_lines)


def candidate_dirs_to_increase_available_space_for_update(console_lines: List[str]) -> List[Directory]:
  # wayup: avoid parsing the console twice
  min_size = missing_space_for_update(console_lines)

  def atLeastMin_totalSizePredicate(d: Directory) -> bool:
    return d.total_size() >= min_size

  directory = parse_directory_from_console_trace(console_lines)
  candidates = find_subdirs_matching_predicate(directory, atLeastMin_totalSizePredicate)
  return list(map(lambda d: d.total_size(), candidates))


def size_of_smallest_candidate_dir_to_increase_available_space_for_update(console_lines: List[str]) -> int:
  # wayup: avoid parsing the console twice
  necessary_space = missing_space_for_update(console_lines)

  def atLeastNecessarySpace_totalSizePredicate(d: Directory) -> bool:
    return d.total_size() >= necessary_space

  directory = parse_directory_from_console_trace(console_lines)
  candidates = find_subdirs_matching_predicate(directory, atLeastNecessarySpace_totalSizePredicate)

  return min(map(lambda d: d.total_size(), candidates))


def read_lines(file: str) -> List[str]:
  with open(file, 'r') as reader:
    return reader.read().splitlines()


if __name__ == '__main__':
  sum_up_to_threshold = sum_directory_sizes_up_to_threshold_from_file('./puzzle-input.txt')
  print(
    f'Sum of the size of the directories with a total size under {DIR_SIZE_SEARCH_MAX_THRESHOLD}: {sum_up_to_threshold}')

  console_lines = read_lines('./puzzle-input.txt')
  sum_up_to_threshold = size_of_smallest_candidate_dir_to_increase_available_space_for_update(
    console_lines)
  used = used_space(console_lines)
  available = available_space(console_lines)
  print()
  print(f'Used space: {used}')
  print(f'Available space: {available}')
  print(f'Necessary space: {UPDATE_SIZE - available}')
  print(f'Size of the smallest directory to increase the available space for the update: {sum_up_to_threshold}')
