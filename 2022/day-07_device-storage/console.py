from typing import List
from files import Directory, File, Node

CD_PREFIX = '$ cd '
ROOT_STR = '/'
PARENT_STR = '..'


class ConsoleTrace:
  def __init__(self, command: str, output: List[str]):
    self.command = command
    self.output = output


# wayup: improve name with the notions of "tree" and "root" for example
def parse_directory_from_console_trace(console_trace_lines: List[str]):
  # In the example, the trace starts with a `cd /` command,
  # so the root directory '/' is expected to be the initial working directory.
  root = Directory('/')
  working_dir = root

  for line in console_trace_lines:
    # The `ls` commands are skipped, in the example they always follow a `cd` command, this is relied upon.

    if is_node_description(line):
      working_dir.add_children(parse_node(line))

    elif is_cd_command(line):
      working_dir = get_new_working_dir(line, root, working_dir)

  return root


def get_new_working_dir(cd_command: str, root: Directory, current_working_dir: Directory) -> Directory:
  target = get_cd_target(cd_command)
  if target == ROOT_STR:
    return root
  elif target == PARENT_STR:
    return current_working_dir.parent
  else:
    # wayup: check that the target is a Directory
    return current_working_dir.find_child_by_name(target)


def is_cd_command(line: str) -> bool:
  return line[:len(CD_PREFIX)] == CD_PREFIX


def get_cd_target(line: str) -> str:
  # wayup: trim trailing whitespaces
  return line[len(CD_PREFIX):]


def is_node_description(line: str) -> bool:
  return len(line) >= 1 and line[0] != '$'


def parse_node(line: str) -> Node:
  # wayup: avoid splitting names that contain spaces
  size_or_dir_str, name = line.split(' ')

  if size_or_dir_str == 'dir':
    return Directory(name)

  else:
    # wayup: check that it's an int and otherwise raise an error
    return File(name, int(size_or_dir_str))
