import copy
import os
from typing import List, Tuple, Iterable
import matplotlib.pyplot as plt

LEFT, RIGHT, UP, DOWN = 'L', 'R', 'U', 'D'
DIRECTIONS = [LEFT, RIGHT, UP, DOWN]
Direction = str

# wayup: avoid this debug globals, integrate them better
OPTION_PRINT_GRID = True
rotations_for_display = 0


class Position:
  """
  The column coordinate is positive to the right.
  The row  coordinate is positive to the bottom.
  """

  def __init__(self, col_x: int, row_y: int):
    self.col = col_x
    self.row = row_y

  @classmethod
  def from_col_row(cls, col_row: Tuple[int, int]) -> 'Position':
    return cls(col_row[0], col_row[1])

  @classmethod
  def from_multiple_col_row(cls, col_row_list: Iterable[Tuple[int, int]]) -> List['Position']:
    return list(map(Position.from_col_row, col_row_list))

  def __eq__(self, other: 'Position'):
    # wayup: extract a parse_tuple() function
    if isinstance(other, tuple):
      if len(other) != 2:
        return False
      else:
        # wayup: make sure that comparing a (col,row) tuple to a Position is intuitive
        #  with respect to the order of the coordinates in the tuple
        other = Position(other[0], other[1])
    return self.col == other.col and self.row == other.row

  def __str__(self):
    return f'(col={self.col}, row={self.row})'

  def __repr__(self):
    return f'Position(col={self.col}, row={self.row})'

  def __hash__(self):
    return (self.col, self.row).__hash__()

  def __iter__(self):
    return (self.col, self.row).__iter__()

  def __getitem__(self, item):
    return (self.col, self.row)[item]


class Move:
  def __init__(self, direction: Direction, distance: int = 1):
    self.direction = Move._valid_direction(direction)
    self.distance = Move._valid_distance(distance)

  def __str__(self):
    """Human-readable representation of the move."""
    return f'{self.direction} {self.distance}'

  def __repr__(self) -> str:
    """Machine-parsable representation of the move."""
    return f'Move(direction={self.direction}, distance={self.distance})'

  @staticmethod
  def _valid_direction(direction: Direction) -> Direction:
    if direction not in DIRECTIONS:
      raise ValueError(f'Invalid direction: {direction}')
    return direction

  @staticmethod
  def _valid_distance(distance: int) -> int:
    if distance < 0:
      raise ValueError(f'Distance must be positive or zero, got {distance}')
    return distance

  @staticmethod
  def parse(dir_dist: str) -> 'Move':
    direction, distance = dir_dist.split(' ')
    return Move(direction, int(distance))

  @staticmethod
  def parse_list(head_moves_str: List[str]) -> List['Move']:
    return [Move.parse(move_str) for move_str in head_moves_str]


class Grid:
  EMPTY_GRID = ''

  HEAD_MARK = 'H'
  TAIL_CURRENT_MARK = 'T'
  TAIL_VISITED_MARK = '#'
  TAIL_START_MARK = 's'
  EMPTY_CELL_MARK = '.'

  ORIGIN_POS = Position(0, 0)

  def __init__(self, rope: 'Rope' = None):
    self.rope = rope
    self._visible_head = True
    self._visible_tail = True

  def with_visible_head(self, state: bool = True) -> 'Grid':
    new_grid = copy.deepcopy(self)
    new_grid._visible_head = state
    return new_grid

  def print_with_tail_visited_pos_count(self):
    """Prints a human-readable representation of the grid."""
    print(str(self))
    print(f'({self.rope.tail.trace_set_length})')
    print()

  # tail_trace: List[Tuple[int, int]] | List[Position]
  def __str__(self):
    """Human-readable representation of the grid."""
    if not self.rope:
      return Grid.EMPTY_GRID

    self._build_marked_array()

    return self._array_to_str()

  def __repr__(self):
    # WAYUP: return a machine representation
    return self.__str__()

  def _build_marked_array(self) -> None:

    # # DEBUG: Rotate the positions, wayup: integrate it better
    # tail_positions = rotate_clockwise(tail_positions, rotations_for_display)
    # head = rotate_clockwise([head], rotations_for_display)[0] if head else None

    self._init_array(self._visible_positions())
    self._mark_tail_then_head()

  def _visible_positions(self):
    # always include the origin, wayup: use a special mark for it
    # wayup: use _visible_head and _visible_tail for the head and tail inclusion
    return [Grid.ORIGIN_POS, self._head_pos, self._tail_pos] + self._tail_trace

  def _init_array(self, positions):
    col_domain = [pos.col for pos in positions]
    self._min_col = min(col_domain)
    self._max_col = max(col_domain)
    self._col_size = self._max_col - self._min_col + 1

    row_domain = [pos.row for pos in positions]
    self._min_row = min(row_domain)
    self._max_row = max(row_domain)
    self._row_size = self._max_row - self._min_row + 1

    self._array = [[Grid.EMPTY_CELL_MARK for _ in range(self._col_size)]
                   for _ in range(self._row_size)]

  def _mark_tail_then_head(self):
    # Mark the tail trace
    # wayup: make the mark-or-not logic more intuitive with respect to the _visible_tail attribute
    self._mark_tail_trace()

    # Mark the tail, especially when there is no trace
    # wayup: check or enforce consistency between the tail trace and the tail position
    if self._visible_tail:
      self._mark_positions(Grid.TAIL_CURRENT_MARK, self._tail_pos)

    # Mark the head, which overrides the tail, so it must be printed after.
    if self._visible_head:
      self._mark_positions(Grid.HEAD_MARK, self._head_pos)

  def _mark_tail_trace(self):
    if self._tail_trace:
      # Mark the visited positions
      self._mark_positions(Grid.TAIL_VISITED_MARK, *self._tail_trace)

      # Mark the start and last tail positions
      self._mark_positions(Grid.TAIL_START_MARK, first(self._tail_trace))

      # The tail overrides the start position, so it must be printed after.
      self._mark_positions(Grid.TAIL_CURRENT_MARK, last(self._tail_trace))

  def _mark_positions(self, mark: str, *positions):
    """
    The rows and columns are offset based on the min row and min col (negative or zero)
    to handle negative position coordinates.
    """
    for pos in positions:
      self._array[pos.row - self._min_row][pos.col - self._min_col] = mark

  @property
  def _head_pos(self) -> Position:
    return self.rope.head.pos

  @property
  def _tail_pos(self) -> Position:
    return self.rope.tail.pos

  @property
  def _tail_trace(self) -> List[Position]:
    return self.rope.tail.trace

  def _array_to_str(self) -> str:
    lines = (''.join(row) for row in self._array)
    return os.linesep.join(lines)


class Rope:
  def __init__(self, knot_labels: List[str]):
    """
    :param knot_labels: from head to tail, single characters
    :raise ValueError: if the knot labels are not valid, i.e. an empty list or not all different
    """
    self.knots = [Knot(label) for label in Rope._validated_labels(knot_labels)]
    # self.knots_history = [copy.deepcopy(self.knots)]

  # pos: Position | Tuple[int, int]
  def with_head_at(self, pos) -> 'Rope':
    new_rope = copy.deepcopy(self)
    new_rope.head.pos = Position(pos[0], pos[1])
    return new_rope

  # *tail_positions: Position | Tuple[int, int]
  # wayup: expand tail_positions if it as a single list element containing a the positions
  def with_tail_trace(self, *tail_positions) -> 'Rope':
    new_rope = copy.deepcopy(self)
    new_rope.tail.trace = [Position(pos[0], pos[1]) for pos in tail_positions]
    new_rope.tail.pos = Position(tail_positions[-1][0], tail_positions[-1][1])  # wayup: simplify
    return new_rope

  # wayup: add tests for this
  @staticmethod
  def _validated_labels(knot_labels: List[str]) -> List[str]:
    if not knot_labels:
      raise ValueError('Empty knot labels')
    if len(knot_labels) != len(set(knot_labels)):
      raise ValueError('Knot labels must be all different')
    return knot_labels

  @property
  def head(self) -> 'Knot':
    return self.knots[0]

  @property
  def tail(self) -> 'Knot':
    return self.knots[-1]

  @property
  def tail_visited_positions(self):
    return set(self.tail.trace)

  # wayup: avoid mutating the current rope
  def move_head(self, move: Move) -> None:
    if OPTION_PRINT_GRID:
      print(f'=== {move.direction} {move.distance} ===')

    for _ in range(move.distance):
      self.unit_head_move(move.direction)
      # self.knots_history += [copy.deepcopy(self.knots)]

    if OPTION_PRINT_GRID:
      Grid(self).print_with_tail_visited_pos_count()

  def unit_head_move(self, direction) -> None:
    # Move the head knot
    self.head.move(direction)

    # Move the following knots one after the other
    # This implementation should allow for the possibility of a rope with more than 2 knots
    # (wayup: move on to day 9 part 2 for which it will be needed)
    for leading, following in zip(self.knots, self.knots[1:]):
      following.follow(leading)

  def apply_moves(self, head_moves: List[Move]) -> None:
    for move in head_moves:
      self.move_head(move)


class Knot:
  def __init__(self, label: str):
    """
    :raise ValueError: if the knot label is not valid, i.e. not a single character
    """
    if len(label) != 1:
      raise ValueError('Knot label must be a single character')  # wayup: add a test for this
    self.label = label
    self._pos = Position(0, 0)  # todo evolve
    self.trace_enabled = False
    self.trace = []

  @property
  def pos(self) -> Position:
    return self._pos

  @pos.setter
  def pos(self, new_pos: Position) -> None:
    """
    Set the knot position to the given new position.

    This adds the current position to the trace if the trace is enabled
    """
    self._pos = new_pos
    if self.trace_enabled:
      self.trace += [self.pos]

  def enable_trace(self) -> None:
    """
    Enable the trace of the knot position.

    This adds the current position to the trace.
    """
    self.trace_enabled = True
    self.trace += [self.pos]

  @property
  def trace_set_length(self) -> int:
    """
    :return: the number of distinct positions in the trace
    """
    # wayup: add a warning if the trace is disabled
    return len(set(self.trace))

  # wayup: return a new Knot instead of mutating the current one, handle the containing Rope accordingly,
  #  or leave it as is
  def move(self, direction: Direction) -> None:
    """
    Move the knot position by 1 unit in the given direction.
    :param direction:    the direction in which to move
    :return:     the new position
    """
    if direction == LEFT:
      self.pos.col -= 1
    elif direction == RIGHT:
      self.pos.col += 1
    elif direction == UP:
      self.pos.row -= 1
    elif direction == DOWN:
      self.pos.row += 1
    else:
      raise ValueError(f'Invalid direction: {direction}')

  def __str__(self) -> str:
    return f'{self.label} at {self.pos}'

  def __repr__(self) -> str:
    """Machine-parsable representation of the object."""
    return f'Knot(label={self.label}, pos={self.pos})'

  # wayup: return a new Knot instead of mutating the current one, handle the containing Rope accordingly,
  #  or leave it as is
  def follow(self, leading_knot: 'Knot') -> None:
    """
    Move the knot position to follow the given head knot.
    :param leading_knot: the head knot
    """

    tail_row, tail_col = self.pos
    head_row, head_col = leading_knot.pos

    if self.pos == leading_knot.pos:
      return

    def get_follow_head_step(tail_coord: int, head_coord: int, min_dist: int) -> int:
      coord_diff = head_coord - tail_coord
      coord_dist = abs(coord_diff)

      assert coord_dist <= 2, f'The steps should be run 1 position at a time, such that the coordinates distance are never greater than 2, got {coord_dist} for tail coord {tail_coord} and head coord {head_coord}'

      if coord_dist >= min_dist:
        return coord_diff // coord_dist if coord_dist > 0 else 0
      else:
        return 0

    col_step = get_follow_head_step(tail_col, head_col, 2)
    row_step = get_follow_head_step(tail_row, head_row, 2)

    # Diagonal move to realign vertically
    if row_step != 0 and tail_row != head_row:
      col_step = get_follow_head_step(tail_col, head_col, 1)

    # Diagnostic move to realign horizontally
    if col_step != 0 and tail_col != head_col:
      row_step = get_follow_head_step(tail_row, head_row, 1)

    # The pos should not be mutated since it is stored in the trace, wayup: make Knot safer for this
    # self.pos.col = tail_row + row_step
    # self.pos.row = tail_col + col_step
    self.pos = Position(tail_row + row_step, tail_col + col_step)


def trace_visited_positions(head_moves_str: List[str]) -> List[Position]:
  """
  The head and tail are assumed to start at the same position.

  The tail follows the head one position at a time, even if the head moves more than one position.

  :param head_moves_str:  the list of moves of the head,
  each move represents a direction and a distance to move in that direction, for example 'R 2' means move right 2 positions.

  :raises ValueError: if the head moves are invalid

  :return: the list of positions visited by the tail
  """

  # Argument parsing and validation
  head_moves = Move.parse_list(head_moves_str)

  # Start position
  rope = Rope(['H', 'T'])
  rope.tail.enable_trace()  # wayup: Rope(lables).enable_trace('T') or .enable_tail_trace() and return a Rope object

  if OPTION_PRINT_GRID:
    print('=== INITIAL GRID ===')
    Grid(rope).print_with_tail_visited_pos_count()

  rope.apply_moves(head_moves)

  # # get tail trace through knots_history
  # tail_trace = set(map(lambda _knots: last(_knots).pos, rope.knots_history))
  # assert set(tail_trace) == set(
  #   rope.tail.trace), f'The tail trace from the rope history should be the same as the tail trace, got \n' \
  #                     f'{tail_trace}\n' \
  #                     f' {rope.tail.trace}'
  return rope.tail.trace


def count_visited_positions(head_moves_str: List[str]) -> int:
  """
  :return: the number of positions visited by the tail at least once
  """
  return len(set(trace_visited_positions(head_moves_str)))


def rotate_move_strings_clockwise(moves_str: List[str]) -> List[str]:
  parsed_moves = [Move.parse(move_str) for move_str in moves_str]
  rotated_moves = rotate_moves_clockwise(parsed_moves)
  return [str(move) for move in rotated_moves]


def rotate_moves_clockwise(moves: List[Move]) -> List[Move]:
  return [Move(rotate_direction_clockwise(move.direction), move.distance) for move in moves]


def rotate_direction_clockwise(direction: Direction) -> Direction:
  if direction == LEFT:
    return UP
  elif direction == UP:
    return RIGHT
  elif direction == RIGHT:
    return DOWN
  elif direction == DOWN:
    return LEFT
  else:
    raise ValueError(f'Invalid direction: {direction}')


# wayup: Use the position class instead of tuples
def rotate_positions_clockwise(positions: List[Tuple[int, int]]) -> List[Tuple[int, int]]:
  return [(-row, col) for col, row in positions]


# wayup: add tests
def rotate_clockwise(positions: List[Position], times=1) -> List[Position]:
  # 1) Handle negative times count
  # 2) Avoid unnecessary rotations when times is greater than 4
  times = times % 4

  # Terminating branch
  if times == 0:
    return positions

  # Recursive branch
  else:
    rotated = [Position(pos.row, -pos.col) for pos in positions]
    return rotate_clockwise(rotated, times - 1)


def first(indexed):
  return indexed[0]


def last(indexed):
  return indexed[-1]


def read_file_lines(file: str) -> List[str]:
  with open(file, 'r') as reader:
    return reader.read().splitlines()


if __name__ == '__main__':
  # PART 1
  OPTION_PRINT_GRID = False
  answer1 = count_visited_positions(read_file_lines('./puzzle-input.txt'))
  print(f'{answer1=}')

  # ATTEMPT TO DEBUG THE RESULT
  # OPTION_PRINT_GRID = False
  # head_moves_str = read_file_lines('./puzzle-input.txt')
  # head_moves_str = head_moves_str[:len(head_moves_str) // 68]
  # for i in range(4):
  #   print(os.linesep + f'======== RUN {i} =========')
  #   rotations_for_display = -i
  #   answer1 = count_visited_positions(head_moves_str)
  #   print(f'{answer1=}')
  #   head_moves_str = rotate_move_strings_clockwise(head_moves_str)

  # PART 2
  rope = Rope(['H', '1', '2', '3', '4', '5', '6', '7', '8', '9'])
  rope.tail.enable_trace()
  rope.apply_moves(Move.parse_list(read_file_lines('./puzzle-input.txt')))
  answer2 = len(rope.tail_visited_positions)
  print(f'{answer2=}')
