import re
from contextlib import redirect_stdout
from io import StringIO
from typing import Set, Callable, Any
from unittest import TestCase, main
from rope_bridge import *

EXAMPLE_MOVES_STR = [
  'R 4',
  'U 4',
  'L 3',
  'D 1',
  'R 4',
  'D 1',
  'L 5',
  'R 2',
]

HEADER_LINES_REGEXP = r'^='
COUNT_LINES_REGEXP = r'^\('


def capture_stdout_lines(func_under_test: Callable[[], Any]) -> List[str]:
  """
  Capture the lines printed on the standard output by the function under test.

  The leading and trailing empty lines are removed.
  The captured output is printed back on the regular standard output.
  """
  with redirect_stdout(StringIO()) as output_io:
    func_under_test()
  output_str = output_io.getvalue().strip()
  print(output_str)
  return output_str.splitlines()


def keep_grid_only(lines):
  return remove_count_lines(
    keep_grid_and_counts(lines))


def keep_grid_and_counts(lines):
  return remove_header_lines(lines)


def remove_header_lines(lines):
  """
  This method should be maintained in case new lines other than the grid are printed.
  """
  return [line
          for line in lines
          if not re.match(HEADER_LINES_REGEXP, line)]


def remove_count_lines(lines):
  """
  This method should be maintained in case new lines other than the grid are printed.
  """
  return [line
          for line in lines
          if not re.match(COUNT_LINES_REGEXP, line)]


###############
# Topic notes :
#
# T Tail
# H Head
#
# T and H must be touching either orthogonally, diagonally or overlapping
#
# If they aren't, the tail follows the head
# - orthogonally if they are vertically or horizontally aligned
# - diagonally otherwise
#
# The objective is to determine the position of the tail for each 1-step move of the head.
# Then count the number of cells the tail visited at least once.
###############


class CountVisitedPositions(TestCase):
  def test_tailOnlyVisitedStartPosition_whenHeadDoesNotMove_noValue(self):
    # No move value
    self.assertEqual(1, count_visited_positions([]))

  def test_tailOnlyVisitedStartPosition_whenHeadDoesNotMove_valueWithZeroPos(self):
    # Move values of 0 positions
    self.assertEqual(1, count_visited_positions(['L 0']))
    self.assertEqual(1, count_visited_positions(['R 0']))
    self.assertEqual(1, count_visited_positions(['U 0']))
    self.assertEqual(1, count_visited_positions(['D 0']))

  def test_tailOnlyVisitedStartPosition_whenHeadMovesOfOne(self):
    self.assertEqual(1, count_visited_positions(['L 1']))
    self.assertEqual(1, count_visited_positions(['R 1']))
    self.assertEqual(1, count_visited_positions(['U 1']))
    self.assertEqual(1, count_visited_positions(['D 1']))

  def test_tailVisitedTwoPos_whenHeadMovesOfTwoPos(self):
    self.assertEqual(2, count_visited_positions(['L 2']))
    self.assertEqual(2, count_visited_positions(['R 2']))
    self.assertEqual(2, count_visited_positions(['U 2']))
    self.assertEqual(2, count_visited_positions(['D 2']))

  def test_tailVisitedThreePos_whenHeadMovesOfThreePos(self):
    for d in DIRECTIONS:
      self.assertEqual(3, count_visited_positions([f'{d} 3']))

  def test_diagonalFollowing(self):
    self.assertEqual(3, count_visited_positions([
      'R 2',
      'U 2',
    ]))

  def test_diagonalFollowingThenDistanceOne_horizontally(self):
    self.assertEqual(3, count_visited_positions([
      'R 2',
      'U 2',
      'D 1',  # same position
      'R 1',  # distance 1 so the tail should not move
    ]))

  def test_diagonalFollowingThenDistanceOne_vertically(self):
    self.assertEqual(3, count_visited_positions([
      'U 2',
      'R 2',
      'L 1',  # same position
      'U 1',  # distance 1 so the tail should not move
    ]))

  def test_example(self):
    self.assertEqual(13, count_visited_positions([
      'R 4',
      'U 4',
      'L 3',
      'D 1',
      'R 4',
      'D 1',
      'L 5',
      'R 2',
    ]))

  def test_example_clockwise_once(self):
    moves = rotate_move_strings_clockwise(EXAMPLE_MOVES_STR)
    self.assertEqual(13, count_visited_positions(moves))

  def test_example_clockwise_twice(self):
    moves = rotate_move_strings_clockwise(
      rotate_move_strings_clockwise(EXAMPLE_MOVES_STR))
    self.assertEqual(13, count_visited_positions(moves))

  def test_example_clockwise_thrice(self):
    moves = rotate_move_strings_clockwise(
      rotate_move_strings_clockwise(
        rotate_move_strings_clockwise(EXAMPLE_MOVES_STR)))
    self.assertEqual(13, count_visited_positions(moves))


class RotateMoveStringsClockwise(TestCase):
  def test_empty(self):
    self.assertEqual([], rotate_move_strings_clockwise([]))

  def test_one_direction(self):
    self.assertEqual(['D 1'], rotate_move_strings_clockwise(['R 1']))

  def test_four_directions_with_different_distances(self):
    result = rotate_move_strings_clockwise(['U 1', 'R 2', 'D 3', 'L 4'])
    expected = ['R 1', 'D 2', 'L 3', 'U 4']
    self.assertEqual(expected, result)

  def test_rotate_four_times_same_as_initial(self):
    initial_moves = ['U 1', 'R 2', 'D 3', 'L 4']
    result = rotate_move_strings_clockwise(
      rotate_move_strings_clockwise(
        rotate_move_strings_clockwise(
          rotate_move_strings_clockwise(initial_moves))))
    self.assertEqual(initial_moves, result)


class TraceVisitedPositions(TestCase):
  def test_tailOnlyVisitedStartPosition_whenHeadDoesNotMove_noValue(self):
    # No move value
    self.assertEqual({(0, 0)}, set(trace_visited_positions([])))

  def test_tailOnlyVisitedStartPosition_whenHeadDoesNotMove_valueWithZeroPos(self):
    # Move values of 0 positions
    self.assertVisitedPositionsInFourDirections({(0, 0)}, ['R 0'])

  def test_tailOnlyVisitedStartPosition_whenHeadMovesOfOne(self):
    self.assertVisitedPositionsInFourDirections({(0, 0)}, ['R 1'])

  def test_tailVisitedTwoPos_whenHeadMovesOfTwoPos(self):
    self.assertVisitedPositionsInFourDirections({(0, 0), (1, 0)}, ['R 2'])

  def test_tailVisitedThreePos_whenHeadMovesOfThreePos(self):
    self.assertVisitedPositionsInFourDirections({(0, 0), (1, 0), (2, 0)}, ['R 3'])

  def test_diagonalFollowing(self):
    self.assertVisitedPositionsInFourDirections({(0, 0), (1, 0), (2, -1)}, ['R 2', 'U 2'])

  def test_scenario_to_debug(self):
    moves = ['L 2',  # single row HTs -> 2 positions visited by the tail
             'R 2',  # single row .TH -> 2 positions visited by the tail
             'D 2']  # two rows   .Ts, ..H then .#s, ..T, ..H
    expected_positions = {(0, 0), (-1, 0), (0, 1)}
    self.assertVisitedPositionsInFourDirections(expected_positions, moves)

  def assertVisitedPositionsInFourDirections(self, expected_positions: Set[Tuple[int, int]], moves: List[str]):
    for _ in range(4):
      self.assertEqual(set(expected_positions), set(trace_visited_positions(moves)))
      moves = rotate_move_strings_clockwise(moves)
      expected_positions = rotate_positions_clockwise(expected_positions)

  def test_first_grid_is_printed(self):
    moves = ['R 1']
    lines = capture_stdout_lines(lambda: trace_visited_positions(moves))
    lines = keep_grid_only(lines)
    self.assertFirstElements(['H', ''], lines)  # with a line separator before the next grid

  def test_last_grid_is_printed(self):
    moves = ['R 2', 'U 2']
    lines = capture_stdout_lines(lambda: trace_visited_positions(moves))
    lines = keep_grid_only(lines)
    self.assertLastElements(['..H', '..T', 's#.'], lines)

  def test_first_count_is_printed(self):
    moves = ['R 1']
    lines = capture_stdout_lines(lambda: trace_visited_positions(moves))
    lines = keep_grid_and_counts(lines)
    self.assertFirstElements(['H', '(1)'], lines)

  def test_last_count_is_printed(self):
    moves = ['R 1']
    lines = capture_stdout_lines(lambda: trace_visited_positions(moves))
    lines = keep_grid_and_counts(lines)
    self.assertLastElements(['(1)'], lines)

  # wayup: find a way to extract this assertion behavior for reuse (idem for assertLastElements)
  def assertFirstElements(self, expected, elements):
    actual = elements[:len(expected)]
    self.assertEqual(expected, actual)

  def assertLastElements(self, expected, elements):
    actual = elements[-len(expected):]
    self.assertEqual(expected, actual)


class RotatePositionsClockwise(TestCase):
  def test_empty(self):
    self.assertEqual([], rotate_positions_clockwise([]))

  def test_0_0(self):
    self.assertEqual([(0, 0)], rotate_positions_clockwise([(0, 0)]))

  def test_1_0(self):
    self.assertEqual([(0, 1)], rotate_positions_clockwise([(1, 0)]))

  def test_0_1(self):
    self.assertEqual([(-1, 0)], rotate_positions_clockwise([(0, 1)]))

  def test_n1_0(self):
    self.assertEqual([(0, -1)], rotate_positions_clockwise([(-1, 0)]))

  def test_0_n1(self):
    self.assertEqual([(1, 0)], rotate_positions_clockwise([(0, -1)]))

  def test_1_2(self):
    self.assertEqual([(-2, 1)], rotate_positions_clockwise([(1, 2)]))

  def test_n1_2(self):
    self.assertEqual([(-2, -1)], rotate_positions_clockwise([(-1, 2)]))

  def test_1_n2(self):
    self.assertEqual([(2, 1)], rotate_positions_clockwise([(1, -2)]))

  def test_n1_n2(self):
    self.assertEqual([(2, -1)], rotate_positions_clockwise([(-1, -2)]))

  def test_greater_coordinates(self):
    self.assertEqual([(0, 20)], rotate_positions_clockwise([(20, 0)]))
    self.assertEqual([(-30, 0)], rotate_positions_clockwise([(0, 30)]))
    self.assertEqual([(-10, 5)], rotate_positions_clockwise([(5, 10)]))

  def test_same_order_with_multiple_positions(self):
    result = rotate_positions_clockwise([(20, 0),
                                         (0, 30),
                                         (5, 10)])
    expected = [(0, 20),
                (-30, 0),
                (-10, 5)]
    self.assertEqual(expected, result)

  def test_outputForSuccessiveDirections_leftUpRightDown(self):
    positions = [(0, 0), (-1, 0), (-2, 0)]
    print(positions)

    positions = rotate_positions_clockwise(positions)
    print(positions)
    self.assertEqual([(0, 0), (0, -1), (0, -2)], positions)

    positions = rotate_positions_clockwise(positions)
    print(positions)
    self.assertEqual([(0, 0), (1, 0), (2, 0)], positions)

    positions = rotate_positions_clockwise(positions)
    print(positions)
    self.assertEqual([(0, 0), (0, 1), (0, 2)], positions)


class DisplayVisitedTailPositions(TestCase):
  def test_empty(self):
    self.assertEqual('', str(Grid()))

  def test_start(self):
    rope = Rope(['H', 'T'])
    grid = Grid(rope).with_visible_head(False)
    self.assertEqual('T', str(grid))

  def test_one_move_right(self):
    rope = Rope(['H', 'T']).with_tail_trace((0, 0), (1, 0))
    grid = Grid(rope).with_visible_head(False)
    self.assertEqual('sT', str(grid))

  def test_two_moves_right(self):
    rope = Rope(['H', 'T']).with_tail_trace((0, 0), (1, 0), (2, 0))
    grid = Grid(rope).with_visible_head(False)
    self.assertEqual('s#T', str(grid))

  def test_one_move_up(self):
    rope = Rope(['H', 'T']).with_tail_trace((0, 0), (0, -1))
    grid = Grid(rope).with_visible_head(False)
    self.assertEqual(['T',
                      's'], str(grid).splitlines())

  def test_two_moves_up(self):
    rope = Rope(['H', 'T']).with_tail_trace((0, 0), (0, -1), (0, -2))
    grid = Grid(rope).with_visible_head(False)
    self.assertEqual(['T',
                      '#',
                      's'], str(grid).splitlines())

  def test_come_back_on_previous_position(self):
    rope = Rope(['H', 'T']).with_tail_trace((0, 0), (1, 0), (2, 0), (2, -1), (2, 0))
    grid = Grid(rope).with_visible_head(False)
    self.assertEqual(['..#',
                      's#T'], str(grid).splitlines())

  def test_bigger(self):
    rope = Rope(['H', 'T']).with_tail_trace((0, 0), (1, -1), (3, -2))
    grid = Grid(rope).with_visible_head(False)
    self.assertEqual(['...T',
                      '.#..',
                      's...'], str(grid).splitlines())

  def test_one_move_left(self):
    rope = Rope(['H', 'T']).with_tail_trace((0, 0), (-1, 0))
    grid = Grid(rope).with_visible_head(False)
    self.assertEqual('Ts', str(grid))

  def test_one_move_down(self):
    rope = Rope(['H', 'T']).with_tail_trace((0, 0), (0, 1))
    grid = Grid(rope).with_visible_head(False)
    self.assertEqual(['s',
                      'T'], str(grid).splitlines())

  def test_one_move_left_then_jump_right(self):
    rope = Rope(['H', 'T']).with_tail_trace((0, 0), (-1, 0), (2, 0))
    grid = Grid(rope).with_visible_head(False)
    self.assertEqual('#s.T', str(grid))

  def test_one_move_down_then_jump_up(self):
    rope = Rope(['H', 'T']).with_tail_trace((0, 0), (0, 1), (0, -2))
    grid = Grid(rope).with_visible_head(False)
    self.assertEqual(['T',
                      '.',
                      's',
                      '#'], str(grid).splitlines())

  def test_accept_Position_objects(self):
    rope = Rope(['H', 'T']).with_tail_trace(Position(0, 0), Position(1, 0), Position(1, -1))
    grid = Grid(rope).with_visible_head(False)
    self.assertEqual(['.T',
                      's#'], str(grid).splitlines())

  def test_display_head_final_position_existing_col(self):
    rope = Rope(['H', 'T']) \
      .with_head_at((2, 0)) \
      .with_tail_trace((0, 0), (1, 0), (2, 0), (3, 0))
    grid = Grid(rope)
    self.assertEqual('s#HT', str(grid))

  def test_display_head_final_position_new_positive_col(self):
    rope = Rope(['H', 'T']) \
      .with_head_at((3, 0)) \
      .with_tail_trace((0, 0), (1, 0), (2, 0))
    grid = Grid(rope)
    self.assertEqual('s#TH', str(grid))

  def test_display_head_final_position_new_negative_col(self):
    rope = Rope(['H', 'T']) \
      .with_head_at((-1, 0)) \
      .with_tail_trace((0, 0), (1, 0), (2, 0))
    grid = Grid(rope)
    self.assertEqual('Hs#T', str(grid))

  def test_display_head_final_position_new_positive_row(self):
    rope = Rope(['H', 'T']) \
      .with_head_at((2, -1)) \
      .with_tail_trace((0, 0), (1, 0), (2, 0))
    grid = Grid(rope)
    self.assertEqual(['..H', 's#T'], str(grid).splitlines())

  def test_display_head_final_position_new_negative_row(self):
    rope = Rope(['H', 'T']) \
      .with_head_at((1, 1)) \
      .with_tail_trace((0, 0), (1, 0), (2, 0))
    grid = Grid(rope)
    self.assertEqual(['s#T', '.H.'], str(grid).splitlines())

  def test_head_overlaps_tail(self):
    rope = Rope(['H', 'T']) \
      .with_head_at((2, 0)) \
      .with_tail_trace((0, 0), (1, 0), (2, 0))
    grid = Grid(rope)
    self.assertEqual('s#H', str(grid))

  def test_head_overlaps_tail_and_start(self):
    rope = Rope(['H', 'T']) \
      .with_head_at((0, 0)) \
      .with_tail_trace((0, 0))
    grid = Grid(rope)
    self.assertEqual('H', str(grid))

  def test_head_as_a_Position_object(self):
    rope = Rope(['H', 'T']) \
      .with_head_at(Position(2, 0)) \
      .with_tail_trace((0, 0), (1, 0))
    grid = Grid(rope)
    self.assertEqual('sTH', str(grid))


# wayup: add test for various cases (with and without head trace)
class GridPrintTest(TestCase):

  def test_head_over_tail_over_start(self):
    rope = Rope(['H', 'T'])
    rope.head.pos = Position(0, 0)
    rope.tail.pos = Position(0, 0)
    grid = Grid(rope)
    result = keep_grid_only(capture_stdout_lines(lambda: grid.print_with_tail_visited_pos_count()))
    self.assertEqual(['H'], result)

  def test_head_over_start(self):
    rope = Rope(['H', 'T'])
    rope.head.pos = Position(0, 0)
    rope.tail.pos = Position(1, 0)
    grid = Grid(rope)
    result = keep_grid_only(capture_stdout_lines(lambda: grid.print_with_tail_visited_pos_count()))
    self.assertEqual(['HT'], result)

  def test_tail_over_start(self):
    rope = Rope(['H', 'T'])
    rope.head.pos = Position(1, 0)
    rope.tail.pos = Position(0, 0)
    grid = Grid(rope)
    result = keep_grid_only(capture_stdout_lines(lambda: grid.print_with_tail_visited_pos_count()))
    self.assertEqual(['TH'], result)

  def test_noStart_tail_head_horizontalPos_sequence(self):
    rope = Rope(['H', 'T'])
    rope.head.pos = Position(2, 0)
    rope.tail.pos = Position(1, 0)
    grid = Grid(rope)
    result = keep_grid_only(capture_stdout_lines(lambda: grid.print_with_tail_visited_pos_count()))
    self.assertEqual(['.TH'], result)

  def test_noStart_tail_head_horizontalNeg_sequence(self):
    rope = Rope(['H', 'T'])
    rope.head.pos = Position(-2, 0)
    rope.tail.pos = Position(-1, 0)
    grid = Grid(rope)
    result = keep_grid_only(capture_stdout_lines(lambda: grid.print_with_tail_visited_pos_count()))
    self.assertEqual(['HT.'], result)

  def test_noStart_tail_head_verticalPos_sequence(self):
    rope = Rope(['H', 'T'])
    rope.head.pos = Position(0, 2)  # Visually speaking, -2 means above -1 and 0.
    rope.tail.pos = Position(0, 1)
    grid = Grid(rope)
    result = keep_grid_only(capture_stdout_lines(lambda: grid.print_with_tail_visited_pos_count()))
    self.assertEqual(['.', 'T', 'H'], result)

  def test_noStart_tail_head_verticalNeg_sequence(self):
    rope = Rope(['H', 'T'])
    rope.head.pos = Position(0, -2)  # Visually speaking, -2 means above -1 and 0.
    rope.tail.pos = Position(0, -1)
    grid = Grid(rope)
    result = keep_grid_only(capture_stdout_lines(lambda: grid.print_with_tail_visited_pos_count()))
    self.assertEqual(['H', 'T', '.'], result)

  def test_one(self):
    rope = Rope(['H', 'T'])
    rope.head.pos = Position(5, 0)
    rope.tail.pos = Position(2, 0)
    rope.tail.trace = [Position(0, 0), Position(3, 0), Position(4, 0)]
    grid = Grid(rope)

    grid.print_with_tail_visited_pos_count()
    # todo: add assertions


class RopeWithMultipleKnotsTest(TestCase):

  def test_part2_example1_move1(self):
    rope = Rope(['H', '1', '2', '3', '4', '5', '6', '7', '8', '9'])
    rope.tail.enable_trace()

    rope.move_head(Move(RIGHT, 4))

    self.assertEqual({Position(0, 0)}, rope.tail_visited_positions)

  def test_part2_example1_allMoves(self):
    rope = Rope(['H', '1', '2', '3', '4', '5', '6', '7', '8', '9'])
    rope.tail.enable_trace()

    rope.move_head(Move(RIGHT, 4))
    rope.move_head(Move(UP, 4))
    rope.move_head(Move(LEFT, 3))
    rope.move_head(Move(DOWN, 1))
    rope.move_head(Move(RIGHT, 4))
    rope.move_head(Move(DOWN, 1))
    rope.move_head(Move(LEFT, 5))
    rope.move_head(Move(RIGHT, 2))

    self.assertEqual({Position(0, 0)}, rope.tail_visited_positions)

  def test_part2_example2_allMoves(self):
    rope = Rope(['H', '1', '2', '3', '4', '5', '6', '7', '8', '9'])
    rope.tail.enable_trace()

    rope.apply_moves(Move.parse_list(['R 5',
                                      'U 8',
                                      'L 8',
                                      'D 3',
                                      'R 17',
                                      'D 10',
                                      'L 25',
                                      'U 20']))

    self.assertEqual(36, len(rope.tail_visited_positions))


if __name__ == '__main__':
  main()
