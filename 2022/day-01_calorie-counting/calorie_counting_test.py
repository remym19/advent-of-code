from unittest import TestCase
from calorie_counting import *


class Top3CumulatedTotalTest(TestCase):
  def test_1elf_1food_1000cals(self):
    self.assert_top3cumulated_total_for_file(1000, 'test_data/1-elf_1-food_1000-cals.txt')

  def test_1elf_1food_2000cals(self):
    self.assert_top3cumulated_total_for_file(2000, 'test_data/1-elf_1-food_2000-cals.txt')

  def test_1elf_2foods_3000cals(self):
    self.assert_top3cumulated_total_for_file(3000, 'test_data/1-elf_2-food_3000-cals.txt')

  def test_2elves_1foodEach_2000calsEach(self):
    self.assert_top3cumulated_total_for_file(4000, 'test_data/2-elves_1-food-each_2000-cals-each.txt')

  def test_4elves_1foodEach_1000calsEach(self):
    # special case: when the third and fourth have the same total
    # then only count the third (in addition to the first and second), not the fourth
    self.assert_top3cumulated_total_for_file(3000, 'test_data/4-elves_1-food-each_1000-cals-each.txt')

  def test_5elves_severalFoodsEach(self):
    self.assert_top3cumulated_total_for_file(5000 + 4000 + 6000, 'test_data/5-elves_multi-foods_6000-max-cals-last.txt')

  def assert_top3cumulated_total_for_file(self, expected_max, input_file):
    self.assertEqual(expected_max, top3_cumulated_total(input_file))


class MaxTotalOverElvesTest(TestCase):

  def test_1elf_1food_1000cals(self):
    self.assert_max_for_file(1000, 'test_data/1-elf_1-food_1000-cals.txt')

  def test_1elf_1food_2000cals(self):
    self.assert_max_for_file(2000, 'test_data/1-elf_1-food_2000-cals.txt')

  def test_1elf_2foods_3000cals(self):
    self.assert_max_for_file(3000, 'test_data/1-elf_2-food_3000-cals.txt')

  def test_2elves_1foodEach_4000maxCalsSecond(self):
    self.assert_max_for_file(4000, 'test_data/2-elves_1-food-each_4000-max-cals-second.txt')

  def test_2elves_1foodEach_4000maxCalsFirst(self):
    self.assert_max_for_file(4000, 'test_data/2-elves_1-food-each_4000-max-cals-first.txt')

  def test_3elves_multiFoods_5000maxCalMiddle(self):
    self.assert_max_for_file(5000, 'test_data/3-elves_multi-foods_5000-max-cals-middle.txt')

  def test_5elves_multiFoods_6000maxCalLast(self):
    self.assert_max_for_file(6000, 'test_data/5-elves_multi-foods_6000-max-cals-last.txt')

  def assert_max_for_file(self, expected_max, input_file):
    answer = max_total_over_elves(input_file)
    self.assertEqual(expected_max, answer)
