LINE_SEPARATOR = '\n'
ELF_SEPARATOR = 2 * LINE_SEPARATOR


def top3_cumulated_total(file):
  elves_as_lines = content_of(file).split(ELF_SEPARATOR)
  totals = map(get_elf_total, elves_as_lines)
  biggest_to_lowest = sorted(totals, reverse=True)
  top3_totals = biggest_to_lowest[0:3]
  return sum(top3_totals)


def max_total_over_elves(file):
  elves_as_lines = content_of(file).split(ELF_SEPARATOR)
  totals = map(get_elf_total, elves_as_lines)
  return max(totals)


def get_elf_total(elves_as_lines):
  foods_as_strings = elves_as_lines.splitlines()
  foods = map(int, foods_as_strings)
  total = sum(foods)
  return total


def content_of(input_file):
  with open(input_file, 'r') as reader:
    file_content = reader.read()
  return file_content


if __name__ == '__main__':
  max_total_answer = max_total_over_elves('puzzle_input.txt')
  top3_cumulated_total_answer = top3_cumulated_total('puzzle_input.txt')
  print(f'max_total : {max_total_answer}')
  print(f'top3_cumulated_total : {top3_cumulated_total_answer}')
