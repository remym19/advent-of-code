from unittest import TestCase, main
from camp_cleanup import *


class OverlappingAssignmentsTest(TestCase):
  def test_separate(self):
    self.assert_expected_overlaps(0, ['1-4,5-6'])

  def test_partially_overlapping_1(self):
    self.assert_expected_overlaps(1, ['1-4,4-6'])

  def test_partially_overlapping_2(self):
    self.assert_expected_overlaps(1, ['1-4,2-6'])

  def test_partially_overlapping_3(self):
    self.assert_expected_overlaps(1, ['1-4,4-6'])

  def test_partially_overlapping_4(self):
    self.assert_expected_overlaps(1, ['6-10,3-6'])

  def test_partially_overlapping_5(self):
    self.assert_expected_overlaps(1, ['4-10,3-6'])

  def test_same(self):
    self.assert_expected_overlaps(1, ['2-5,2-5'])

  def test_first_assignment_included(self):
    self.assert_expected_overlaps(1, ['12-14,10-20'])

  def test_second_assignment_included(self):
    self.assert_expected_overlaps(1, ['12-14,13-13'])

  def test_three_pairs_three_overlapping_assignments(self):
    self.assert_expected_overlaps(3, ['124-478,10-150', '10-100, 20-30', '1000-2000, 1500-1501'])

  def test_included_in_other_pair(self):
    self.assert_expected_overlaps(0, ['1-10,100-150', '110-120,200-300'])

  def test_count_overlaps_from_file(self):
    self.assertEqual(4, count_pairs_with_overlapping_assignments_from_file('test_data/example-count-2.txt'))

  def assert_expected_overlaps(self, expected, assignment_pairs: List[str]):
    self.assertEqual(expected, count_pairs_with_overlapping_assignments(assignment_pairs))


class IncludedAssignmentTest(TestCase):
  def test_separate(self):
    self.assert_expected_inclusions_in(0, ['1-4,5-6'])

  def test_partially_overlapping_1(self):
    self.assert_expected_inclusions_in(0, ['1-4,4-6'])

  def test_partially_overlapping_2(self):
    self.assert_expected_inclusions_in(0, ['1-4,2-6'])

  def test_partially_overlapping_3(self):
    self.assert_expected_inclusions_in(0, ['1-4,4-6'])

  def test_partially_overlapping_4(self):
    self.assert_expected_inclusions_in(0, ['6-10,3-6'])

  def test_partially_overlapping_5(self):
    self.assert_expected_inclusions_in(0, ['4-10,3-6'])

  def test_same(self):
    self.assert_expected_inclusions_in(1, ['2-5,2-5'])

  def test_first_assignment_included(self):
    self.assert_expected_inclusions_in(1, ['12-14,10-20'])

  def test_second_assignment_included(self):
    self.assert_expected_inclusions_in(1, ['12-14,13-13'])

  def test_three_pairs_two_included_assignments(self):
    self.assert_expected_inclusions_in(2, ['124-478,10-150', '10-100, 20-30', '1000-2000, 1500-1501'])

  def test_included_in_other_pair(self):
    self.assert_expected_inclusions_in(0, ['1-10,100-150', '110-120,200-300'])

  def test_count_inclusions_from_file(self):
    self.assertEqual(2, count_pairs_with_included_assignment_from_file('test_data/example-count-2.txt'))

  def assert_expected_inclusions_in(self, expected, assignment_pairs: List[str]):
    self.assertEqual(expected, count_pairs_with_included_assignment(assignment_pairs))


if __name__ == '__main__':
  main()
