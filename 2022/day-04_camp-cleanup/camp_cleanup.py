from typing import List, Tuple

AssignmentType = Tuple[int, int]
PairType = Tuple[AssignmentType, AssignmentType]


def count_pairs_with_overlapping_assignments_from_file(filename: str):
  with open(filename, 'r') as reader:
    pairs_str_list = reader.read().splitlines()
  count = count_pairs_with_overlapping_assignments(pairs_str_list)
  return count


def count_pairs_with_overlapping_assignments(pairs_str_list: List[str]):
  pairs = map(parse_assignments_pair, pairs_str_list)
  pair_matchings = map(assignments_overlap, pairs)  # booleans

  individual_counts = map(int, pair_matchings)  # int(True) == 1 in Python.
  total_count = sum(individual_counts)
  return total_count


def assignments_overlap(assignments_pair) -> bool:
  def starts_within(assignment1, assignment2) -> bool:
    a1_start = assignment1[0]
    return is_within(a1_start, assignment2)

  def ends_within(assignment1, assignment2) -> bool:
    a1_end = assignment1[1]
    return is_within(a1_end, assignment2)

  def is_within(value, bounds_inclusive: Tuple[int, int]) -> bool:
    start, end = bounds_inclusive
    return start <= value <= end

  a1, a2 = assignments_pair
  return starts_within(a1, a2) or ends_within(a1, a2) or \
    starts_within(a2, a1) or ends_within(a2, a1)


def count_pairs_with_included_assignment_from_file(filename: str):
  with open(filename, 'r') as reader:
    pairs_str_list = reader.read().splitlines()
  count = count_pairs_with_included_assignment(pairs_str_list)
  return count


def count_pairs_with_included_assignment(pairs_str_list: List[str]):
  pairs = map(parse_assignments_pair, pairs_str_list)
  pair_matchings = map(has_included_assignment, pairs)  # booleans
  individual_counts = map(int, pair_matchings)  # int(True) == 1 in Python.
  count = sum(individual_counts)
  return count


def has_included_assignment(assignments_pair) -> bool:
  def is_included_within(assignment1, assignment2) -> bool:
    a1_start, a1_end = assignment1
    a2_start, a2_end = assignment2
    return a2_start <= a1_start and a1_end <= a2_end

  a1, a2 = assignments_pair
  return is_included_within(a1, a2) or is_included_within(a2, a1)


def parse_assignments_pair(pair_str) -> PairType:
  def parse_pair(pair_str) -> Tuple[str, str]:
    # pair_str format example : '2-4,5-12'
    assignments_str = pair_str.split(',')
    assert len(assignments_str) == 2
    return assignments_str[0], assignments_str[1]

  def parse_assignment(assignment_str) -> AssignmentType:
    # assignment_str format example : '14-190'
    bounds = list(map(int, assignment_str.split('-')))
    assert len(bounds) == 2

    start_bound = bounds[0]
    end_bound_inclusive = bounds[1]
    assert start_bound <= end_bound_inclusive
    return start_bound, end_bound_inclusive

  a1_str, a2_str = parse_pair(pair_str)
  a1 = parse_assignment(a1_str)
  a2 = parse_assignment(a2_str)
  return a1, a2


if __name__ == '__main__':
  inclusions_count = count_pairs_with_included_assignment_from_file('./puzzle_input.txt')
  print(f'Count of pairs with one assignment included in the other: {inclusions_count}')

  overlaps_count = count_pairs_with_overlapping_assignments_from_file('./puzzle_input.txt')
  print(f'Count of pairs with overlapping assignments: {overlaps_count}')
